<<?php echo $settings->item_html_tag; ?> id="woopack-product-<?php echo get_the_ID(); ?>" <?php echo WooPack_Helper::render_post_class( $product, $settings, $settings->layout_style ); ?> data-product-id="<?php echo get_the_ID(); ?>">

	<?php

	$custom_layout = (array) $settings->custom_layout;
	$custom_layout_html = apply_filters( 'woopack_product_custom_layout_html', $custom_layout['html'] );

	do_action( 'woopack_product_custom_layout_before_content', $settings );
	echo do_shortcode( FLThemeBuilderFieldConnections::parse_shortcodes( stripslashes( $custom_layout_html ) ) );
	do_action( 'woopack_product_custom_layout_after_content', $settings );
	
	?>

</<?php echo $settings->item_html_tag; ?>>