<?php if ( ! isset( $settings->layout ) || 'custom' !== $settings->layout ) : ?>

<?php
$button_class_selector	= ".fl-node-$id .woocommerce .products .woopack-product-action a.button,
							.fl-node-$id .woocommerce .products .woopack-product-action a.button.alt,
							.fl-node-$id .woocommerce .products .woopack-product-action a.add_to_cart_button,
							.fl-node-$id .woocommerce .products .woopack-product-action a.add_to_cart_button.alt,
							.fl-node-$id .woocommerce .products .woopack-product-action a.added_to_cart,
							.fl-node-$id .woocommerce .products .woopack-product-action .button,
							.fl-node-$id .woocommerce .products .woopack-product-action .button.alt,
							.fl-node-$id .woocommerce .products .woopack-product-action button,
							.fl-node-$id .woocommerce .products .woopack-product-action button.alt";

// ******************* Border *******************
// Button Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'button_border_group',
	'selector' 		=> $button_class_selector,
) );

// ******************* Padding *******************
// Button Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'button_padding',
	'selector' 		=> $button_class_selector,
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'button_padding_top',
		'padding-right' 	=> 'button_padding_right',
		'padding-bottom' 	=> 'button_padding_bottom',
		'padding-left' 		=> 'button_padding_left',
	),
) );

// ******************* Typography *******************
// Button Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'button_typography',
	'selector' 		=> $button_class_selector,
) );
?>


.fl-node-<?php echo $id; ?> .woocommerce ul.products .woopack-product-action,
.fl-node-<?php echo $id; ?> .woocommerce div.products .woopack-product-action {
	<?php if ( 'default' == $settings->button_alignment ) { ?>
		<?php WooPack_Helper::print_css( 'text-align', $default_align ); ?>
	<?php } else { ?>
		<?php WooPack_Helper::print_css( 'text-align', $settings->button_alignment ); ?>
	<?php }; ?>
}
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .woopack-qty-input,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .variations_form .quantity {
	<?php if ( isset( $settings->qty_input ) ) { ?>
		<?php if ( 'no' == $settings->qty_input ) { ?>
			display: none;
		<?php } ?>
		<?php if ( 'above_button' == $settings->qty_input ) { ?>
			display: block;
			margin: 0 auto;
		<?php } else { ?>
			display: inline-block;
		<?php } ?>
		<?php if ( 'before_button' == $settings->qty_input ) { ?>
			margin-right: 2px;
		<?php } ?>
		<?php if ( 'after_button' == $settings->qty_input ) { ?>
			margin-left: 2px;
		<?php } ?>
		<?php if ( 'full_width' == $settings->button_width ) { ?>
			display: block;
			margin: 0 auto;
		<?php } ?>
	<?php } ?>
	width: 60px;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .woopack-qty-input input.qty,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .variations_form .quantity input.qty {
	width: 100%;
}

.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .variations_form table,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .grouped_form table {
	<?php if ( 'default' == $settings->product_align || 'center' == $settings->product_align ) { ?>
	margin: 0 auto;
	<?php } ?>
	<?php if ( 'right' == $settings->product_align ) { ?>
	margin: 0 0 0 auto;
	<?php } ?>
	margin-bottom: 10px;
}

.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .grouped_form div.quantity {
	margin-right: 10px;
    margin-bottom: 5px;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .grouped_form .woocommerce-grouped-product-list-item__label label {
	margin-right: 10px;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .grouped_form .woocommerce-grouped-product-list-item__price .amount {
	display: inline-block;
	margin-bottom: 5px;
}

<?php
// Product variations
include WOOPACK_DIR . 'includes/product-variations.css.php';
?>

<?php echo $button_class_selector; ?> {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color ); ?>

	<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom, 'px' ); ?>

	<?php if ( 'full_width' == $settings->button_width ) { ?>
		width: 100%;
	<?php } elseif ( 'custom' == $settings->button_width ) { ?>
		width: <?php echo $settings->button_width_custom; ?>%;
	<?php } else { ?>
		width: auto;
	<?php } ?>

	text-align: center;
	-webkit-transition: 0.2s ease-in-out;
		-moz-transition: 0.2s ease-in-out;
			transition: 0.2s ease-in-out;
}
<?php if ( 'full_width' == $settings->button_width ) { ?>
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.button.alt.added,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action .button.alt.added {
	width: 50%;
}
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.added_to_cart,
.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.added_to_cart.alt {
	width: 48%;
	margin-left: 0;
}
<?php } ?>
.fl-node-<?php echo $id; ?> .woocommerce ul.products .woopack-product-action a.button:hover,
.fl-node-<?php echo $id; ?> .woocommerce div.products .woopack-product-action a.button:hover,
.fl-node-<?php echo $id; ?> .woocommerce ul.products .woopack-product-action .button:hover,
.fl-node-<?php echo $id; ?> .woocommerce div.products .woopack-product-action .button:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color_hover ); ?>
}

@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.button,
	.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.add_to_cart_button,
	.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.added_to_cart {
		<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_medium, 'px' ); ?>
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.button,
	.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.add_to_cart_button,
	.fl-node-<?php echo $id; ?> .woocommerce .products .woopack-product-action a.added_to_cart {
		<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_responsive, 'px' ); ?>
	}
}

<?php endif; ?>