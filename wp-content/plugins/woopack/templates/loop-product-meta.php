<?php
/**
 * Loop Product Meta.
 *
 * This template can be overridden by copying it to yourtheme/woopack/templates/loop-product-meta.php.
 *
 * HOWEVER, on occasion WooPack will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @package WooPack/Templates
 * @version 1.3.0
 */
?>

<?php
FLBuilderModel::default_settings( $settings, array(
	'select_taxonomy'		    => 'none',
	'show_taxonomy_custom_text'	=> 'no',
	'taxonomy_custom_text'	    => __( 'Category:', 'woopack' ),
) );
?>

<?php do_action( 'woopack_loop_before_product_meta_wrap', $settings, $product ); ?>

<div class="woopack-product-meta">
	<div class="product_meta">
		<?php do_action( 'woocommerce_product_meta_start' ); ?>

		<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
			<span class="sku_wrapper"><?php esc_html_e( 'SKU:', 'woopack' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woopack' ); ?></span></span>
		<?php endif; ?>

		<span class="posted_in">
		<?php
		if ( $settings->show_taxonomy_custom_text == 'yes' ) {
			echo $settings->taxonomy_custom_text;
		} else {
			echo __( 'Category: ', 'woopack' );
		}
			$pos_type_slug 	= $settings->post_type;
			$product_tax 	= $settings->select_taxonomy;
			$product_terms	= wp_get_post_terms( $product->get_id(), $product_tax, array(
				'fields' => 'all',
			) );

			$terms = '';

			if ( ! is_wp_error( $product_terms ) && ! empty( $product_terms ) && $product_tax != 'none' ) {
				foreach ( $product_terms as $term ) {

					$term_link = get_term_link( $term );
					$terms .= '<a href="' . $term_link . '">' . $term->name . '</a>, ';

				}
				$terms = rtrim( $terms, ', ' );
				echo $terms;
			}
		?>
		</span>
		<?php do_action( 'woocommerce_product_meta_end' ); ?>
	</div>
</div>

<?php do_action( 'woopack_loop_after_product_meta_wrap', $settings, $product ); ?>
