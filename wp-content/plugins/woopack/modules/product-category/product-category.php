<?php
/**
* @class WooPackProductCategory
*/
class WooPackProductCategory extends FLBuilderModule {
	/**
	* Constructor function for the module. You must pass the
	* name, description, dir and url in an array to the parent class.
	*
	* @method __construct
	*/
	public function __construct() {
		parent::__construct( array(
			'name'          => __( 'Product Category', 'woopack' ),
			'description'   => __( 'Addon to display Product Category.', 'woopack' ),
			'group'         => WooPack_Helper::get_modules_group(),
			'category'      => WOOPACK_CAT,
			'dir'           => WOOPACK_DIR . 'modules/product-category/',
			'url'           => WOOPACK_URL . 'modules/product-category/',
			'editor_export' => true, // Defaults to true and can be omitted.
			'enabled'       => true, // Defaults to true and can be omitted.
		));
	}
	public function filter_settings( $settings, $helper ) {

		// Handle old Button Settings.
		$settings = filter_product_button_settings( $settings );

		// Handle old Form Border setting.
		$settings = WooPack_Fields::handle_border_field( $settings, array(
			'category_border_style'	 => array(
				'type' => 'style'
			),
			'category_border_width'  => array(
				'type' => 'width'
			),
			'category_border_color'  => array(
				'type' => 'color'
			),
			'category_border_radius' => array(
				'type' => 'radius'
			),
			'box_shadow_h'           => array(
				'type' => 'shadow_horizontal',
			),
			'box_shadow_v'           => array(
				'type' => 'shadow_vertical',
			),
			'box_shadow_blur'        => array(
				'type' => 'shadow_blur',
			),
			'box_shadow_spread'      => array(
				'type' => 'shadow_spread',
			),
			'box_shadow_color'       => array(
				'type' => 'shadow_color',
			)
		), 'box_border_group' );

		// Handle old Category Title Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'category_title_font'             => array(
				'type' => 'font'
			),
			'category_title_font_size_custom' => array(
				'type'      => 'font_size',
				'condition' => ( isset( $settings->category_title_font_size ) && 'custom' == $settings->category_title_font_size )
			),
			'category_title_line_height'      => array(
				'type' => 'line_height',
			),
			'category_title_text_transform'	  => array(
				'type' => 'text_transform',
			),
		), 'category_title_typography' );

		// Handle old Category Description Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'category_description_font'             => array(
				'type' => 'font'
			),
			'category_description_font_size_custom' => array(
				'type'      => 'font_size',
				'condition' => ( isset( $settings->category_description_font_size ) && 'custom' == $settings->category_description_font_size )
			),
			'category_description_line_height'      => array(
				'type' => 'line_height',
			),
			'category_description_text_transform'	  => array(
				'type' => 'text_transform',
			),
		), 'category_description_typography' );

		return $settings;
	}

	public function get_categories( $args, $display_data ) {
		$all_categories = array();

		if ( 'children_only' === $display_data ) {
			if ( isset( $args['parent'] ) && is_array( $args['parent'] ) && intval( $args['parent'][0] ) > 0 ) {
				$parents = $args['parent'];
				unset( $args['parent'] );
				foreach ( $parents as $parent_id ) {
					$args['child_of'] = $parent_id;
					$tmp_categories   = get_categories( $args );
					if ( count( $tmp_categories ) > 0 ) {
						foreach ( $tmp_categories as $cat ) {
							$all_categories[] = $cat;
						}
					}
				}
			}
		} elseif ( 'default' === $display_data ) {
			if ( isset( $args['parent'] ) && is_array( $args['parent'] ) && intval( $args['parent'][0] ) > 0 ) {
				$parents = $args['parent'];
				unset( $args['parent'] );
				foreach ( $parents as $parent_id ) {
					$args['child_of'] = $parent_id;
					$tmp_categories   = get_categories( $args );
					if ( count( $tmp_categories ) > 0 ) {
						foreach ( $tmp_categories as $cat ) {
							$all_categories[] = $cat;
						}
					}
				}
				unset( $args['child_of'] );
				//include parent also
				$args['hierarchical'] = 0;
				$args['include'] = $parents;

				$tmp_categories = get_categories( $args );
				if ( count( $tmp_categories ) > 0 ) {
					foreach ( $tmp_categories as $cat ) {
						$all_categories[] = $cat;
					}
				}
			} else {
				unset( $args['parent'] );
			}
		} else {
			$all_categories = get_categories( $args );
		}

		if ( empty( $all_categories ) ) {
			$all_categories = get_categories( $args );
		}

		return $all_categories;
	}
}
/**
* Register the module and its form settings.
*/
FLBuilder::register_module( 'WooPackProductCategory', array(
	'structure'  => array(
		'title'    => __( 'Structure', 'woopack' ),
		'sections' => array(
			'config'           => array(
				'title'  => __( 'Structure', 'woopack' ),
				'fields' => array(
					'category_style'		=> array(
						'type'    => 'select',
						'label'   => __( 'Style', 'woopack' ),
						'default' => 'style-0',
						'options' => array(
							'style-0' => __( 'Default', 'woopack' ),
							'style-1' => __( 'Style 1', 'woopack' ),
							'style-2' => __( 'Style 2', 'woopack' ),
							'style-3' => __( 'Style 3', 'woopack' ),
							'style-4' => __( 'Style 4', 'woopack' ),
							'style-5' => __( 'Style 5', 'woopack' ),
							'style-6' => __( 'Style 6', 'woopack' ),
							'style-7' => __( 'Style 7', 'woopack' ),
							'style-8' => __( 'Style 8', 'woopack' ),
						),
						'toggle'  => array(
							'style-0' => array(
								'fields'   => array( 'category_bg_color_hover', 'category_show_button', 'category_count_text' ),
								'sections' => array( 'padding' ),
							),
							'style-1' => array(
								'fields'   => array( 'style_1_animation', 'category_bg_color_hover', 'category_bg_opacity', 'category_title_hover_color', 'category_count_hover_color', 'category_show_button', 'category_show_desc' ),
								'sections' => array( 'padding', 'des_margin', 'category_description_fonts' ),
							),
							'style-2' => array(
								'fields'   => array( 'style_2_animation', 'category_show_button', 'category_show_desc' ),
								'sections' => array( 'sh_height', 'padding' ),
							),
							'style-3' => array(
								'fields'   => array( 'style_3_animation', 'category_bg_color_hover', 'category_bg_opacity', 'category_title_hover_color', 'category_count_hover_color', 'category_show_button', 'category_show_desc' ),
								'sections' => array( 'separator', 'padding', 'des_margin', 'category_description_fonts' ),
							),
							'style-4' => array(
								'fields'   => array( 'category_bg_color_hover', 'category_bg_opacity', 'category_title_hover_color', 'category_count_hover_color', 'category_show_button', 'category_show_desc' ),
								'sections' => array( 'separator', 'padding', 'des_margin', 'category_description_fonts' ),
							),
							'style-5' => array(
								'fields'   => array( 'category_bg_color_hover', 'category_bg_opacity', 'category_show_desc' ),
								'sections' => array( 'separator', 'des_margin', 'category_description_fonts' ),
							),
							'style-6' => array(
								'fields' => array( 'style_6_animation', 'category_bg_color_hover', 'category_bg_opacity', 'category_show_desc' ),
								'sections' => array( 'padding' ),
							),
							'style-7' => array(
								'fields'   => array( 'style_7_animation', 'style_8_animation', 'category_bg_color_hover', 'category_bg_opacity', 'category_title_hover_color', 'category_count_hover_color', 'category_show_button', 'category_show_desc' ),
								'sections' => array( 'separator', 'padding', 'des_margin', 'category_description_fonts' ),
							),
							'style-8' => array(
								'fields'   => array( 'style_8_animation', 'category_bg_color_hover', 'category_bg_opacity', 'category_title_hover_color', 'category_count_hover_color', 'category_show_button', 'category_show_desc' ),
								'sections' => array( 'separator', 'padding', 'des_margin', 'category_description_fonts' ),
							),
						),
					),
					'category_columns'		=> array(
						'type'       => 'unit',
						'label'      => __( 'Columns', 'woopack' ),
						'default'    => '3',
						'slider'	 => true,
						'responsive' => true,
					),
					'category_spacing'		=> array(
						'type'        => 'unit',
						'label'       => __( 'Spacing', 'woopack' ),
						'default'     => '2',
						'units'		  => array('%'),
						'slider'	  => true,
						'responsive'  => true,
						'preview'	  => array(
							'type'		=> 'css',
							'rules'		=> array(
								array(
									'selector'	=> '.woopack-product-category',
									'property'	=> 'margin-right',
									'unit'		=> '%'
								),
								array(
									'selector'	=> '.woopack-product-category',
									'property'	=> 'margin-bottom',
									'unit'		=> '%'
								)
							)
						)
					),
					'category_height'		=> array(
						'type'			=> 'unit',
						'label'			=> __( 'Height', 'woopack' ),
						'default'		=> '300',
						'units'			=> array('px'),
						'slider'		=> true,
						'responsive'	=> true,
					),
					'category_text_align'	=> array(
						'type'    => 'align',
						'label'   => __( 'Content Alignment', 'woopack' ),
						'default' => 'default',
						'options' => array(
							'default' => __( 'Default', 'woopack' ),
							'center'  => __( 'Center', 'woopack' ),
							'left'    => __( 'Left', 'woopack' ),
							'right'   => __( 'Right', 'woopack' ),
						),
					),
					'style_1_animation'		=> array(
						'type'    => 'select',
						'label'   => __( 'Animate content from (on hover)', 'woopack' ),
						'default' => 'left',
						'options' => array(
							'top'    => __( 'Top', 'woopack' ),
							'bottom' => __( 'Bottom', 'woopack' ),
							'left'   => __( 'Left', 'woopack' ),
							'right'  => __( 'Right', 'woopack' ),
						),
					),
					'style_2_animation'		=> array(
						'type'    => 'select',
						'label'   => __( 'Animate content from (on hover)', 'woopack' ),
						'default' => 'bottom',
						'options' => array(
							'top'            => __( 'Top', 'woopack' ),
							'bottom'         => __( 'Bottom', 'woopack' ),
						),
					),
					'style_6_animation'		=> array(
						'type'    => 'select',
						'label'   => __( 'Animate content from (on hover)', 'woopack' ),
						'default' => 'bottom',
						'options' => array(
							'top'    => __( 'Top', 'woopack' ),
							'bottom' => __( 'Bottom', 'woopack' ),
						),
					),
					'style_7_animation'		=> array(
						'type'    => 'select',
						'label'   => __( 'Border on hover', 'woopack' ),
						'default' => 'tr-bl',
						'options' => array(
							'tr-bl' => __( 'Top-Right & Bottom-Left', 'woopack' ),
							'tl-br' => __( 'Top-Left & Bottom-Right', 'woopack' ),
						),
					),
					'style_8_animation'		=> array(
						'type'    => 'select',
						'label'   => __( 'Animation Type', 'woopack' ),
						'default' => 'in',
						'options' => array(
							'in'  => __( 'Fade In', 'woopack' ),
							'out' => __( 'Fade Out', 'woopack' ),
						),
					),
					'transition_speed'		=> array(
						'type'			=> 'unit',
						'label'			=> __( 'Transition Speed', 'woopack' ),
						'units'			=> array('second'),
						'slider'		=> true,
						'default'		=> '0.3',
					),
				),
			),
			'content'          => array(
				'title'     => __( 'Settings', 'woopack' ),
				'collapsed' => true,
				'fields'    => array(
					'category_show_counter' => array(
						'type'    => 'select',
						'label'   => __( 'Show Product Counter?', 'woopack' ),
						'default' => 'yes',
						'options' => array(
							'yes' => __( 'Yes', 'woopack' ),
							'no'  => __( 'No', 'woopack' ),
						),
						'toggle'  => array(
							'yes' => array(
								'sections' => array( 'category_count_fonts' ),
							),
						),
					),
					'category_show_desc' => array(
						'type'    => 'select',
						'label'   => __( 'Show Product Description?', 'woopack' ),
						'default' => 'yes',
						'options' => array(
							'yes' => __( 'Yes', 'woopack' ),
							'no'  => __( 'No', 'woopack' ),
						),
						'toggle'  => array(
							'yes' => array(
								'sections' => array( 'category_description_fonts' ),
							),
						),
					),
					'category_count_text'   => array(
						'type'    => 'text',
						'label'   => __( 'Counter Text', 'woopack' ),
						'default' => __('Product', 'woopack'),
					),
					'category_show_button'  => array(
						'type'    => 'select',
						'label'   => __( 'Show Button?', 'woopack' ),
						'default' => 'yes',
						'options' => array(
							'yes' => __( 'Yes', 'woopack' ),
							'no'  => __( 'No', 'woopack' ),
						),
						'toggle'  => array(
							'yes' => array(
								'tabs'   => array( 'button' ),
								'fields' => array( 'category_button_text' ),
							),
						),
					),
					'category_button_text'  => array(
						'type'    => 'text',
						'label'   => __( 'Button Text', 'woopack' ),
						'default' => __('Shop Now', 'woopack'),
					),
					'category_link_window'  => array(
						'type'    => 'select',
						'label'   => __( 'Link Type', 'woopack' ),
						'default' => 'box',
						'options' => array(
							'box'          => __( 'Entire Box', 'woopack' ),
							'title'        => __( 'Title Only', 'woopack' ),
							'button'       => __( 'Button Only', 'woopack' ),
							'title_button' => __( 'Title + Button', 'woopack' ),
						),
					),
					'category_link_target'  => array(
						'type'    => 'select',
						'label'   => __( 'Link Target', 'woopack' ),
						'options' => array(
							'_self'  => __( 'Same Window', 'woopack' ),
							'_blank' => __( 'New Window', 'woopack' ),
						),
					),
				),
			),
		),
	),
	'content'	 => array(
		'title'		=> __('Content', 'woopack'),
		'file'      => WOOPACK_DIR . 'modules/product-category/includes/settings-content.php',
	),
	'button'     => array(
		'title'    => __( 'Button', 'woopack' ),
		'sections' => woopack_product_button_fields(),
	),
	'style'      => array(
		'title'    => __( 'Style', 'woopack' ),
		'sections' => array(
			'color'      => array(
				'title'  => __( 'Overlay', 'woopack' ),
				'fields' => array(
					'category_bg_color'   		=> array(
						'type'       => 'color',
						'label'      => __( 'Background Color', 'woopack' ),
						'default'    => '',
						'show_reset' => true,
						'show_alpha' => true,
					),
					'category_bg_color_hover'   => array(
						'type'       	=> 'color',
						'label'      	=> __( 'Background Hover Color', 'woopack' ),
						'default'    	=> '',
						'show_reset' 	=> true,
						'show_alpha' 	=> true,
						'preview'		=> array(
							'type'			=> 'none',
						)
					),
					'category_bg_opacity' 		=> array(
						'type'        => 'text',
						'label'       => __( 'Image Opacity', 'woopack' ),
						'default'     => '0.5',
						'description' => __('between 0 to 1', 'woopack'),
						'size'        => '5',
						'preview'		=> array(
							'type'			=> 'css',
							'selector'		=> '.woopack-product-category .product-category-inner .woopack-product-category__img img',
							'property'		=> 'opacity'
						)
					),
				),
			),
			'separator'	 => array(
				'title'		=> __( 'Separator', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'category_separator_color' => array(
						'type'       => 'color',
						'label'      => __( 'Color', 'woopack' ),
						'default'    => 'ffffff',
						'show_reset' => true,
						'show_alpha' => true,
					),
					'category_separator_height' => array(
						'type'			=> 'unit',
						'label'			=> __( 'Height', 'woopack' ),
						'default'		=> '2',
						'slider'		=> true,
						'units'			=> array('px'),
					),
				),
			),
			'border'     => array(
				'title'     => __( 'Border', 'woopack' ),
				'collapsed' => true,
				'fields'    => array(
					'box_border_group'	=> array(
						'type'					=> 'border',
						'label'					=> __('Border Style', 'woopack'),
						'responsive'			=> true,
						'preview'				=> array(
							'type'					=> 'css',
							'selector'				=> '.woopack-product-category',
						),
					),
				),
			),
			'padding'    => array(
				'title'  	=> __( 'Padding', 'woopack' ),
				'collapsed' => true,
				'fields' 	=> array(
					'category_padding'    => array(
						'type'        	=> 'dimension',
						'label'       	=> __( 'Padding', 'woopack' ),
						'default'		=> 10,
						'units'			=> array('px'),
						'responsive'  	=> true,
						'preview'     	=> array(
							'type'     		=> 'css',
							'selector' 		=> '.woopack-product-category .woopack-product-category__content',
							'property' 		=> 'padding',
							'unit'     		=> 'px',
						),
					),
				),
			),
			'margin'     => array(
				'title'  	=> __( 'Margin', 'woopack' ),
				'collapsed' => true,
				'fields' 	=> array(
					'category_margin'    => array(
						'type'        		=> 'dimension',
						'label'       		=> __( 'Margin', 'woopack' ),
						'default'			=> '0',
						'units'				=> array('px'),
						'responsive'  		=> true,
						'preview'     		=> array(
							'type'    	 		=> 'css',
							'selector' 			=> '.woopack-product-category .woopack-product-category__content',
							'property' 			=> 'margin',
							'unit'     			=> 'px',
						),
					),
				),
			),
			'sh_height'  => array(
				'title'  	=> __( 'Shutter Height', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'shutter_height' => array(
						'type'       => 'unit',
						'label'      => __( 'Height', 'woopack' ),
						'default'    => '80',
						'slider'	 => true,
						'units' 	 => array( 'px' ),
						'responsive' => true,
					),
				),
			),
			'des_margin' => array(
				'title'  => __( 'Description Margin', 'woopack' ),
				'fields' => array(
					'des_margin_top' => array(
						'type'       => 'unit',
						'label'      => __( 'Margin Top', 'woopack' ),
						'units'      => array( 'px' ),
						'responsive' => true,
						'slider'     => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__description',
							'property' => 'margin-top',
							'unit'     => 'px',
						),
					),
				),
			),
		),
	),
	'typography' => array(
		'title'    => __( 'Typography', 'woopack' ),
		'sections' => array(
			'category_title_fonts'       => array(
				'title'  => __( 'Category Title', 'woopack' ),
				'fields' => array(
					'category_title_tag'	=> array(
						'type'		=> 'select',
						'label'		=> __('Tag', 'woopack'),
						'default'	=> 'h3',
						'options'	=> array(
							'h1'		=> 'h1',
							'h2'		=> 'h2',
							'h3'		=> 'h3',
							'h4'		=> 'h4',
							'h5'		=> 'h5',
							'h6'		=> 'h6',
						)
					),
					'category_title_typography'  => array(
						'type'       => 'typography',
						'label'      => __( 'Typography', 'woopack' ),
						'responsive' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__title',
						),
					),
					'category_title_color'            => array(
						'type'       => 'color',
						'label'      => __( 'Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__title',
							'property' => 'color',
						),
					),
					'category_title_hover_color'      => array(
						'type'       => 'color',
						'label'      => __( 'Hover Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'none',
						),
					),
				),
			),
			'category_count_fonts'       => array(
				'title'  => __( 'Category Count', 'woopack' ),
				'fields' => array(
					'category_count_font_size'        => array(
						'type'    => 'select',
						'label'   => __( 'Font Size', 'woopack' ),
						'default' => 'default',
						'options' => array(
							'default' => __( 'Default', 'woopack' ),
							'custom'  => __( 'Custom', 'woopack' ),
						),
						'toggle'  => array(
							'custom' => array(
								'fields' => array( 'category_count_font_size_custom' ),
							),
						),
					),
					'category_count_font_size_custom' => array(
						'type'       => 'unit',
						'label'      => __( 'Custom Font Size', 'woopack' ),
						'units'      => array( 'px' ),
						'responsive' => true,
						'slider'     => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__title span,
										.woopack-product-category .product-category-style-0 .woopack-product-category__title_wrapper span',
							'property' => 'font-size',
							'unit'     => 'px',
						),
					),
					'category_count_color'            => array(
						'type'       => 'color',
						'label'      => __( 'Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__title span, .woopack-product-category .product-category-style-0 .woopack-product-category__title_wrapper span',
							'property' => 'color',
						),
					),
					'category_count_hover_color'      => array(
						'type'       => 'color',
						'label'      => __( 'Hover Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'none',
						),
					),
				),
			),
			'category_description_fonts' => array(
				'title'  => __( 'Category Description', 'woopack' ),
				'fields' => array(
					'category_description_typography'           => array(
						'type'       => 'typography',
						'label'      => __( 'Typography', 'woopack' ),
						'responsive' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__description',
						),
					),
					'category_description_color'          => array(
						'type'       => 'color',
						'label'      => __( 'Color', 'woopack' ),
						'show_reset' => true,
						'show_alpha' => true,
						'preview'    => array(
							'type'     => 'css',
							'selector' => '.woopack-product-category .woopack-product-category__description',
							'property' => 'color',
						),
					),
				),
			),
		),
	),
));
