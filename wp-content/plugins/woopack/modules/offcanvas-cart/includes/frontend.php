<?php
	$wrapperClasses = array(
		'woopack-offcanvas-cart',
	);

	$wrapperClasses[] = 'woocommerce';
?>
<div class="<?php echo implode( ' ', $wrapperClasses ); ?>">
	<div class="woopack-cart-button">

		<?php if ( 'before' == $settings->counter_position ) { ?>
			<span class="cart-contents-count-before">
				<span class="cart-counter"><?php echo is_object( WC()->cart ) ? WC()->cart->get_cart_contents_count() : '0'; ?></span>
			</span>
		<?php } ?>

		<a class="woopack-cart-contents <?php echo 'woopack-cart-' . $settings->icon_style;?>" href="#" title="<?php _e( 'View your shopping cart', 'woopack' ); ?>">
			<span class="cart-button-wrap">
			<?php if ( 'icon' == $settings->icon_style ) { ?>

				<?php if ( 'icon' == $settings->icon_type ) { ?>
					<?php if ( '' != $settings->cart_icon ) { ?>
						<span class="cart-contents-icon <?php echo $settings->cart_icon; ?>"></span>
					<?php } else { ?>	
						<span class="cart-contents-icon fa fa-shopping-cart"></span>
					<?php } ?>	
				<?php } elseif ( 'image' == $settings->icon_type ) {?>	
					<?php if ( isset( $settings->cart_image_src ) ) { ?>
						<img src="<?php echo $settings->cart_image_src; ?>" class="cart-contents-image" />
					<?php } ?>
				<?php } ?>

			<?php } elseif ( 'icon_text' == $settings->icon_style ) { ?>
				
				<?php if ( 'icon' == $settings->icon_type ) { ?>
					<?php if ( '' != $settings->cart_icon ) { ?>
						<span class="cart-contents-icon <?php echo $settings->cart_icon; ?>"></span>
					<?php } else { ?>	
						<span class="cart-contents-icon fa fa-shopping-cart"></span>
					<?php } ?>	
				<?php } elseif ( 'image' == $settings->icon_type ) {?>	
					<?php if ( isset( $settings->cart_image_src ) ) { ?>
						<img src="<?php echo $settings->cart_image_src; ?>" class="cart-contents-image"/>
					<?php } ?>
				<?php } ?>

				<span class="cart-contents-text"><?php echo $settings->cart_text; ?></span>
			
			<?php } else { ?>
			
				<span class="cart-contents-text"><?php echo $settings->cart_text; ?></span>
			
			<?php } ?>
			</span>

			<?php if ( 'top' == $settings->counter_position ) { ?>
				<span class="cart-contents-count">
					<span class="cart-counter"><?php echo is_object( WC()->cart ) ? WC()->cart->get_cart_contents_count() : '0'; ?></span>
				</span>
			<?php } ?>

		</a>

		<?php if ( 'after' == $settings->counter_position ) { ?>
			<span class="cart-contents-count-after">
				<span class="cart-counter"><?php echo is_object( WC()->cart ) ? WC()->cart->get_cart_contents_count() : '0'; ?></span>
			</span>
		<?php } ?>

	</div>

	<div id="woopack-cart-<?php echo $id; ?>" class="woocommerce woopack-offcanvas-cart-panel direction-<?php echo $settings->direction; ?>" data-node="<?php echo $id; ?>">
		<div class="woopack-offcanvas-overlay"></div>
		<div class="woopack-offcanvas-inner">
			<div class="woopack-offcanvas-header woopack-clear">
				<div class="woopack-offcanvas-title"></div>
				<div class="woopack-offcanvas-close">
					<span class="fa fa-times"></span>
				</div>
			</div>
			<div class="woopack-offcanvas-content">
				<?php do_action( 'woopack_offcanvas_cart_before_items' ); ?>
				<div class="woopack-cart-items">
					<div class="widget_shopping_cart_content"><?php woocommerce_mini_cart();?></div>
				</div>
				<?php do_action( 'woopack_offcanvas_cart_after_items' ); ?>
			</div>
		</div>
	</div>
	
</div>
