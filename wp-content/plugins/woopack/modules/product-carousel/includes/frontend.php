<?php
if ( ! isset( $settings->item_html_tag ) ) {
	$settings->item_html_tag = $module->get_item_html_tag();
}
if ( ! isset( $settings->layout_style ) ) {
	$settings->layout_style = $module->get_layout_style();
}
if ( ! isset( $settings->woopack ) ) {
	$settings->woopack = true;
}
if ( ! isset( $settings->woopack_post_id ) ) {
	$settings->woopack_post_id = get_the_ID();
}

if ( isset( $settings->product_source ) && 'custom_query' === $settings->data_source ) {
	if ( 'featured' === $settings->product_source ) {
		add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::featured_products', 10, 1 );
	}
	if ( 'best_selling' === $settings->product_source ) {
		add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::best_selling_products', 10, 1 );
	}
	if ( 'sale' === $settings->product_source ) {
		add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::sale_products', 10, 1 );
	}
	if ( 'top_rated' === $settings->product_source ) {
		add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::top_rated_products', 10, 1 );
	}
	if ( 'related' === $settings->product_source ) {
		WooPack_Helper::$settings = $settings;
		add_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::related_products', 10, 1 );
	}
}

$settings->product_carousel = true;

// Get the query data.
$query = FLBuilderLoop::query( $settings );

// Render the posts.
if ( $query->have_posts() ) :

	do_action( 'woopack_before_products_carousel', $settings, $query );

	$paged = ( FLBuilderLoop::get_paged() > 0 ) ? ' woopack-paged-scroll-to' : '';

	if ( function_exists( 'is_product' ) && ! is_product() ) {
		do_action( 'woocommerce_before_shop_loop' );
	}

	?>
	<div class="woopack-products-carousel woopack-layout-<?php echo $module->settings->product_layout; ?> woocommerce">
		<div class="woopack-products products owl-carousel owl-theme">
			<?php
			while ( $query->have_posts() ) {

				$query->the_post();

				$product = wc_get_product( get_the_ID() );

				if ( is_object( $product ) && $product->is_visible() ) {
					$product_data = $product->get_data();

					//include apply_filters( 'woopack_products_carousel_layout_path', WOOPACK_DIR . 'templates/product-layout-' . $module->settings->product_layout . '.php', $settings );
					include apply_filters( 'woopack_products_carousel_layout_path', WooPack_Helper::get_template_file( 'product-layout-' . $settings->product_layout, $settings, $product, 1 ), $settings, $product );
				}
			}
			?>
		</div>
	</div>
	<?php

	if ( function_exists( 'is_product' ) && ! is_product() ) {
		if ( function_exists( 'woocommerce_pagination' ) ) {
			remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
		}
		do_action( 'woocommerce_after_shop_loop' );
	}

	do_action( 'woopack_after_products_carousel', $settings, $query );
endif;


// Render the empty message.
if ( ! $query->have_posts() ) :

?>
<div class="woopack-carousel-empty">
	<p><?php echo isset( $settings->no_results_message ) ? $settings->no_results_message : ''; ?></p>
	<?php if ( isset( $settings->show_search ) && $settings->show_search ) : ?>
	<?php get_search_form(); ?>
	<?php endif; ?>
</div>

<?php

endif;

wp_reset_postdata();

if ( isset( $settings->product_source ) && 'custom_query' === $settings->data_source ) {
	if ( 'featured' === $settings->product_source ) {
		remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::featured_products', 10 );
	}
	if ( 'best_selling' === $settings->product_source ) {
		remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::best_selling_products', 10 );
	}
	if ( 'sale' === $settings->product_source ) {
		remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::sale_products', 10 );
	}
	if ( 'top_rated' === $settings->product_source ) {
		remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::top_rated_products', 10 );
	}
	if ( 'related' === $settings->product_source ) {
		WooPack_Helper::$settings = $settings;
		remove_filter( 'fl_builder_loop_query_args', 'WooPack_Helper::related_products', 10, 1 );
	}
}
?>
