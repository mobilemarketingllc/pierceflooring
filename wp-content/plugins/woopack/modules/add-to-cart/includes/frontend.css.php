<?php
$button_selector 		= ".fl-node-$id .woopack-product-add-to-cart .button,
							.fl-node-$id .woopack-product-add-to-cart a.button,
							.fl-node-$id .woopack-product-add-to-cart a.button.alt,
							.fl-node-$id .woopack-product-add-to-cart a.added_to_cart,
							.fl-node-$id .woopack-product-add-to-cart button,
							.fl-node-$id .woopack-product-add-to-cart button.button,
							.fl-node-$id .woopack-product-add-to-cart button.alt,
							.fl-node-$id .woopack-product-add-to-cart .button.alt,
							.fl-node-$id .woopack-product-add-to-cart button.button.alt.disabled,
							.fl-node-$id .woopack-product-add-to-cart button.button.alt";

$button_selector_hover 	= ".fl-node-$id .woopack-product-add-to-cart a.button:hover,
							.fl-node-$id .woopack-product-add-to-cart a.button.alt:hover,
							.fl-node-$id .woopack-product-add-to-cart a.added_to_cart:hover,
							.fl-node-$id .woopack-product-add-to-cart button:hover,
							.fl-node-$id .woopack-product-add-to-cart .button:hover,
							.fl-node-$id .woopack-product-add-to-cart button.button:hover,
							.fl-node-$id .woopack-product-add-to-cart button.alt:hover,
							.fl-node-$id .woopack-product-add-to-cart .button.alt:hover,
							.fl-node-$id .woopack-product-add-to-cart button.button.alt.disabled:hover,
							.fl-node-$id .woopack-product-add-to-cart button.button.alt:hover";
// Button Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'button_border_group',
	'selector' 		=> $button_selector,
) );

// Border - Hover Settings
if ( ! empty( $settings->button_border_color_hover ) && is_array( $settings->button_border_group ) ) {
	$settings->button_border_group['color'] = $settings->button_border_color_hover;
}

FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'button_border_group',
	'selector' 		=> $button_selector_hover
) );

// Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'button_padding',
	'selector' 		=> $button_selector,
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'button_padding_top',
		'padding-right' 	=> 'button_padding_right',
		'padding-bottom' 	=> 'button_padding_bottom',
		'padding-left' 		=> 'button_padding_left',
	),
) );
// Button Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'button_typography',
	'selector' 		=> $button_selector,
) );
// Ragular Price Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'ragular_price_typography',
	'selector' 		=> ".fl-node-$id .product .amount",
) );
?>
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .add_to_cart_inline {
	border: none !important;
}
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .amount,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart del,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart ins {
	<?php if ( 'no' == $settings->product_price ) { ?>
		display: none !important;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .product {
	text-align: <?php echo $settings->align; ?>;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-product-price {
	margin-bottom: 10px;
	<?php if ( isset( $settings->qty_input ) && 'no' == $settings->qty_input ) { ?>
		display: inline-block;
	<?php } ?>
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-product-action {
	<?php if ( isset( $settings->qty_input ) && 'no' == $settings->qty_input ) { ?>
		display: inline-block;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-product-action .woopack-product-action-inner {
	<?php if ( 'auto' == $settings->button_width ) { ?>
		display: inline-block;
	<?php } else { ?>
		display: block;
	<?php } ?>
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-qty-input,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .variations_form .quantity {
	<?php if ( isset( $settings->qty_input ) ) { ?>
		<?php if ( 'no' == $settings->qty_input ) { ?>
			display: none !important;
		<?php } ?>
	<?php } ?>
	width: 60px;
	float: left;
}
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-qty-input input.qty,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .variations_form .quantity input.qty {
	width: 100%;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .variations_form table.variations,
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart.woocommerce .grouped_form table {
	<?php if ( 'default' == $settings->align || 'center' == $settings->align ) { ?>
	margin: 0 auto;
	<?php } ?>
	<?php if ( 'right' == $settings->align ) { ?>
	margin: 0 0 0 auto;
	<?php } ?>
	margin-bottom: 10px;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .variations_form .label {
	color: inherit;
}
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .variations_form .label label {
	font-size: 12px;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .variations_form .reset_variations {
	margin-left: 5px;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woocommerce-variation-add-to-cart {
	display: inline-block;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woocommerce-variation {
	margin-top: 10px;
	margin-bottom: 10px;
}

<?php if ( $settings->button_width == 'full_width' ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-product-action .woopack-product-action-inner,
	.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-product-action,
	.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woopack-qty-input,
	.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .variations_form .quantity,
	.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .woocommerce-variation-add-to-cart {
		display: block;
		width: 100%;
	}
<?php } ?>

<?php
// Product variations
include WOOPACK_DIR . 'includes/product-variations.css.php';
?>

<?php echo $button_selector; ?> {
	<?php WooPack_Helper::print_css( 'color', $settings->button_color ); ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color ); ?>

	<?php if ( 'yes' == $settings->product_price ) { ?>
		<?php WooPack_Helper::print_css( 'margin-left', $settings->price_spacing, 'px' ); ?>
	<?php } ?>
	<?php if ( 'full_width' == $settings->button_width ) { ?>
		width: 100%;
		margin-left: 0;
		<?php WooPack_Helper::print_css( 'margin-top', $settings->price_spacing, 'px' ); ?>
	<?php } elseif ( 'custom' == $settings->button_width && '' != $settings->button_width_custom ) { ?>
		width: <?php echo $settings->button_width_custom; ?>%;
	<?php } ?>
	text-align: center;
}

<?php echo $button_selector_hover; ?> {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color_hover ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .single_variation_wrap button {
	float: left;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .product .amount {
	<?php WooPack_Helper::print_css( 'color', $settings->ragular_price_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .product del .amount {
	text-decoration: line-through;
}

.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .product ins {
	text-decoration: none;
}
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart .product ins .amount {
	text-decoration: none;
	<?php WooPack_Helper::print_css( 'font-size', $settings->sale_price_font_size, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->sale_price_color ); ?>
}


.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart.woocommerce .grouped_form div.quantity {
	margin-right: 10px;
    margin-bottom: 5px;
}
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart.woocommerce .grouped_form .woocommerce-grouped-product-list-item__label label {
	margin-right: 10px;
}
.fl-node-<?php echo $id; ?> .woopack-product-add-to-cart.woocommerce .grouped_form .woocommerce-grouped-product-list-item__price .amount {
	display: inline-block;
	margin-bottom: 5px;
}