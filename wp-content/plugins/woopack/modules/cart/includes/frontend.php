<?php do_action( 'woopack_before_cart_wrap', $settings ); ?>

<div class="woopack-product-cart">
	<?php do_action( 'woopack_before_cart_content', $settings ); ?>

    <?php
    if ( 'no' === $settings->show_cross_sells ) {
        // Hide Cross Sell field on cart page
        remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );
    }

    if ( 'no' === $settings->show_coupon ) {
        // Hide coupon field on cart page
        add_filter( 'woocommerce_coupons_enabled', '__return_false' );
    }

    echo do_shortcode('[woocommerce_cart]');
    ?>

	<?php do_action( 'woopack_after_cart_content', $settings ); ?>
</div>

<?php do_action( 'woopack_after_cart_wrap', $settings ); ?>
