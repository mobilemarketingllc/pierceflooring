<?php
/**
* @class WooPackCart
*/
class WooPackCart extends FLBuilderModule {
    /**
    * Constructor function for the module. You must pass the
    * name, description, dir and url in an array to the parent class.
    *
    * @method __construct
    */
    public function __construct()
    {
        parent::__construct(array(
            'name' 				=> __('Cart', 'woopack'),
            'description' 		=> __('Addon to display Cart.', 'woopack'),
            'group'             => WooPack_Helper::get_modules_group(),
            'category' 			=> WOOPACK_CAT,
            'dir' 				=> WOOPACK_DIR . 'modules/cart/',
            'url' 				=> WOOPACK_URL . 'modules/cart/',
            'editor_export' 	=> true, // Defaults to true and can be omitted.
            'enabled' 			=> true, // Defaults to true and can be omitted.
        ));
	}
	public function filter_settings( $settings, $helper ) {
		// Handle old Cart Table Border setting.
		$settings = WooPack_Fields::handle_border_field( $settings, array(
			'table_border_style'   => array(
				'type'                  => 'style'
			),
			'table_border_width'   => array(
				'type'                  => 'width'
			),
			'table_border_color'   => array(
				'type'                  => 'color'
			),
			'table_border_radius'  => array(
				'type'                  => 'radius'
			)
		), 'table_border_group' );

		// Handle old Cart Total Border setting.
		$settings = WooPack_Fields::handle_border_field( $settings, array(
			'cart_total_border_style'  	=> array(
				'type'                  	=> 'style'
			),
			'cart_total_border_width'   => array(
				'type'                  	=> 'width'
			),
			'cart_total_border_color'   => array(
				'type'                  	=> 'color'
			),
			'cart_total_border_radius'  => array(
				'type'                  	=> 'radius'
			)
		), 'cart_total_border_group' );

		// Handle old Standard Button Border setting.
		$settings = WooPack_Fields::handle_border_field( $settings, array(
			'standard_button_border_style'	=> array(
				'type'                  		=> 'style'
			),
			'standard_button_border_width'	=> array(
				'type'                  		=> 'width'
			),
			'standard_button_border_color'	=> array(
				'type'                  		=> 'color'
			),
			'standard_button_border_radius'	=> array(
				'type'                  		=> 'radius'
			)
		), 'standard_button_border_group' );

		// Handle old Checkout Border setting.
		$settings = WooPack_Fields::handle_border_field( $settings, array(
			'checkout_border_style'	=> array(
				'type'					=> 'style'
			),
			'checkout_border_width'	=> array(
				'type'					=> 'width'
			),
			'checkout_border_color'	=> array(
				'type'					=> 'color'
			),
			'checkout_border_radius' => array(
				'type'					=> 'radius'
			)
		), 'checkout_border_group' );

		$settings = WooPack_Fields::handle_dimension_field( $settings, 'checkout_padding', 'checkout_padding_top' );
		$settings = WooPack_Fields::handle_dimension_field( $settings, 'checkout_padding', 'checkout_padding_bottom' );
		$settings = WooPack_Fields::handle_dimension_field( $settings, 'checkout_padding', 'checkout_padding_left' );
		$settings = WooPack_Fields::handle_dimension_field( $settings, 'checkout_padding', 'checkout_padding_right' );

		// Handle old Default Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'default_font'   			=> array(
				'type'          			=> 'font'
			),
			'default_font_size_custom'	=> array(
				'type'          			=> 'font_size',
				'condition'     			=> ( isset( $settings->default_font_size ) && 'custom' == $settings->default_font_size )
			),
		), 'default_typography' );
		// Handle old Table Header Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'table_header_font'   			=> array(
				'type'          				=> 'font'
			),
			'table_header_font_size_custom'	=> array(
				'type'          				=> 'font_size',
				'condition'     				=> ( isset( $settings->table_header_font_size ) && 'custom' == $settings->table_header_font_size )
			),
			'table_header_text_transform'	=> array(
				'type'          				=> 'text_transform'
			),
		), 'table_header_typography' );

		// Handle old Cart Totals Title Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'cart_totals_title_font_family'			=> array(
				'type'          						=> 'font'
			),
			'cart_totals_title_font_size_custom'	=> array(
				'type'          						=> 'font_size',
				'condition'     						=> ( isset( $settings->cart_totals_title_font_size ) && 'custom' == $settings->cart_totals_title_font_size )
			),
			'cart_totals_text_transform'			=> array(
				'type'          						=> 'text_transform'
			),
		), 'cart_totals_title_typography' );

		// Handle old Standard Button Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'standard_button_font_family'		=> array(
				'type'          						=> 'font'
			),
			'standard_button_font_size_custom'	=> array(
				'type'          					=> 'font_size',
				'condition'     					=> ( isset( $settings->standard_button_font_size ) && 'custom' == $settings->standard_button_font_size )
			),
			'standard_button_text_transform'	=> array(
				'type'          					=> 'text_transform'
			),
		), 'standard_button_typography' );

		// Handle old Checkout Button Typography setting.
		$settings = WooPack_Fields::handle_typography_field( $settings, array(
			'checkout_button_font_family'		=> array(
				'type'          						=> 'font'
			),
			'checkout_button_font_size_custom'	=> array(
				'type'          					=> 'font_size',
				'condition'     					=> ( isset( $settings->checkout_button_font_size ) && 'custom' == $settings->checkout_button_font_size )
			),
			'checkout_button_text_transform'	=> array(
				'type'          					=> 'text_transform'
			),
		), 'checkout_button_typography' );
		return $settings;
	}
}
/**
* Register the module and its form settings.
*/
FLBuilder::register_module('WooPackCart', array(
    'content'           => array(
        'title'             => __('Content', 'woopack'),
        'sections'          => array(
            'coupon'            => array(
                'title'             => __('Apply Coupon', 'woopack'),
                'fields'            => array(
                    'show_coupon'       => array(
                        'type'              => 'select',
                        'label' 			=> __('Show Coupon?', 'woopack'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no'                => __('No', 'woopack'),
                        ),
                        'toggle'            => array(
                            'yes'               => array(
                                'fields'            => array( 'coupon_text_color'),
                            ),
                        ),
                    ),
                    'coupon_text_color' => array(
                        'type'              => 'color',
                        'label'             => __('Input Text Color', 'woopack'),
                        'default'           => '',
                        'show_reset'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.woocommerce table.shop_table.cart td.actions .coupon .input-text',
                            'property'          => 'color',
                        ),
                    ),
                ),
            ),
            'cross_sells'       => array(
				'title'             => __('Cross Sells', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'show_cross_sells'  => array(
                        'type'              => 'select',
                        'label' 			=> __('Show Cross Sells?', 'woopack'),
                        'default'           => 'yes',
                        'options'           => array(
                            'yes'               => __('Yes', 'woopack'),
                            'no'                => __('No', 'woopack'),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'cart_table_style'  => array(
        'title'             => __('Cart Table', 'woopack'),
        'sections'          => array(
            'box_style'         => array(
                'title'             => __('Box', 'woopack'),
                'fields'            => array(
                    'table_bg_color'        => array(
                        'type'                  => 'color',
                        'label'                 => __('Background Color', 'woopack'),
                        'default'               => '',
                        'show_reset'            => true,
                        'show_alpha'            => true,
                        'preview'               => array(
                            'type' 				 => 'css',
                            'selector'			   => '.woocommerce table.shop_table.cart',
                            'property'			   => 'background-color',
                        ),
                    ),
					'table_border_group'	=> array(
						'type'					=> 'border',
						'label'					=> __('Border Style', 'woopack'),
						'responsive'			=> true,
						'preview'				=> array(
							'type'					=> 'css',
							'selector'				=> '.woocommerce table.shop_table.cart',
						),
					),
                    'table_padding_t_b'     => array(
                        'type'                  => 'unit',
                        'label'                 => __('Top & Bottom Padding', 'woopack'),
						'units'					=> array('px'),
						'slider'				=> true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'rules' 	            => array(
                                array(
                                    'selector'          => '.woocommerce table.shop_table.cart',
                                    'property'          => 'padding-top',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => '.woocommerce table.shop_table.cart',
                                    'property'          => 'padding-bottom',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'            => true,
                    ),
                    'table_padding_r_l'     => array(
                        'type'                  => 'unit',
                        'label'                 => __('Right & Left Padding', 'woopack'),
						'units'					=> array('px'),
						'slider'				=> true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'rules' 	            => array(
                                array(
                                    'selector'          => '.woocommerce table.shop_table.cart',
                                    'property'          => 'padding-right',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => '.woocommerce table.shop_table.cart',
                                    'property'          => 'padding-left',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'            => true,
                    ),
                ),
            ),
            'table_header_style'=> array(
				'title'             => __('Table Heading', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'table_header_padding'      => array(
                        'type'                      => 'unit',
                        'label' 				    => __('Vertical Spacing', 'woopack'),
						'units'						=> array('px'),
						'slider'					=> true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'rules'                     => array(
                                array(
                                    'selector'          => '.woocommerce table.shop_table.cart thead th',
                                    'property'          => 'padding-top',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => '.woocommerce table.shop_table.cart thead th',
                                    'property'          => 'padding-bottom',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'                => true,
                    ),
                    'table_header_border_width' => array(
                        'type'                      => 'unit',
                        'label'                     => __('Separator Width', 'woopack'),
						'units'						=> array('px'),
						'slider'					=> true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce table.shop_table.cart thead th',
                            'property'                  => 'border-bottom-width',
                            'unit'                      => 'px',
                        ),
                        'responsive'                => true,
                    ),
                    'table_header_border_color' => array(
                        'type'                      => 'color',
                        'label'                     => __('Separator Color', 'woopack'),
                        'show_reset'                => true,
                        'default'                   => '',
                    ),
                ),
            ),
            'cart_item_style'   => array(
				'title'             => __('Cart Item', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'cart_item_padding'      => array(
                        'type'                  => 'unit',
                        'label' 				=> __('Vertical Spacing', 'woopack'),
						'units'					=> array('px'),
						'slider'				=> true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'rules' 	            => array(
                                array(
                                    'selector'          => '.woocommerce table.shop_table.cart td',
                                    'property'          => 'padding-top',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => '.woocommerce table.shop_table.cart td',
                                    'property'          => 'padding-bottom',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'                => true,
                    ),
                    'cart_item_odd_color'    => array(
                        'type' 				     => 'color',
                        'label' 			     => __('Odd Item Background Color', 'woopack'),
                        'show_reset' 		     => true,
                        'show_alpha' 		     => true,
                        'default'                => '',
                        'preview'                => array(
                            'type' 				     => 'css',
                            'selector'			     => '.woocommerce tr.woocommerce-cart-form__cart-item.cart_item:nth-child(odd)',
                            'property'               => 'background',
                        ),
                    ),
                    'cart_item_even_color'   => array(
                        'type' 				     => 'color',
                        'label' 			     => __('Even Item Background Color', 'woopack'),
                        'show_reset' 		     => true,
                        'show_alpha' 		     => true,
                        'default'                => '',
                        'preview' 			     => array(
                            'type' 				     => 'css',
                            'selector'			     => '.woocommerce tr.woocommerce-cart-form__cart-item.cart_item:nth-child(even)',
                            'property'      	     => 'background',
                        ),
                    ),
                    'cart_item_border_width' => array(
                        'type'                  => 'unit',
                        'label'                 => __('Separator Width', 'woopack'),
						'units'					=> array('px'),
						'slider'				=> true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woocommerce table.shop_table.cart td',
                            'property'              => 'border-top-width',
                            'unit'                  => 'px',
                        ),
                        'responsive' 			=> true,
                    ),
                    'cart_item_border_color' => array(
                        'type' 				     => 'color',
                        'label' 			     => __('Separator Color', 'woopack'),
                        'show_reset' 		     => true,
                        'show_alpha' 		     => true,
                        'default'                => '',
                    ),
                ),
            ),
            'image_style'       => array(
				'title'             => __('Image', 'woopack'),
				'collapsed'			=> true,
                'fields'            => array(
                    'image_width'       => array(
                        'type'              => 'unit',
                        'label' 			=> __('Width', 'woopack'),
						'units'				=> array('px'),
						'slider'			=> true,
                        'preview' 			=> array(
                            'type' 				=> 'css',
                            'selector'			=> '.woocommerce table.cart .product-thumbnail img',
                            'property'			=> 'width',
                            'unit' 				=> 'px'
                        ),
                        'responsive' 		=> true,
                    ),
                ),
			),
			'qty_style'			=> array(
				'title'				=> __('Quantity Input', 'woopack'),
				'collapsed'			=> true,
				'fields'			=> array(
					'qty_width'			=> array(
						'type'				=> 'unit',
						'label'				=> __('Width', 'woopack'),
						'default'			=> '',
						'units'				=> array('px'),
						'slider'			=> true
					),
					'qty_padding'		=> array(
						'type'				=> 'dimension',
						'label'				=> __('Padding', 'woopack'),
						'default'			=> '',
						'units'				=> array('px'),
						'slider'			=> true
					)
				)
			),
            'product_remove_font'	=> array(
				'title'                 => __('Product Remove Icon', 'woopack'),
				'collapsed'			=> true,
                'fields'                => array(
                    'product_remove_font_size'          => array(
                        'type'                              => 'select',
                        'label'  			                => __('Size', 'woopack'),
                        'default'                           => 'default',
                        'options' 		                    => array(
                            'default' 		                    => __('Default', 'woopack'),
                            'custom'                            => __('Custom', 'woopack'),
                        ),
                        'toggle'                            => array(
                            'custom'                            => array(
                                'fields'                            => array('product_remove_font_size_custom')
                            ),
                        ),
                    ),
                    'product_remove_font_size_custom'   => array(
                        'type'                              => 'unit',
                        'label'                             => __('Custom Size', 'woopack'),
						'units'								=> array('px'),
						'slider'							=> true,
                        'responsive' 			            => true,
                        'preview'                           => array(
                            'type' 					            => 'css',
                            'selector'                          => '.woocommerce table.shop_table.cart td.product-remove a',
                            'property'                          => 'font-size',
                            'unit'                              => 'px',
                        ),
                    ),
                    'product_remove_color'              => array(
                        'type'                              => 'color',
                        'label'                             => __('Color', 'woopack'),
                        'default'                           => '',
                        'show_reset'                        => true,
                    ),
                    'product_remove_color_hover'        => array(
                        'type'                              => 'color',
                        'label'                             => __('Color on Hover', 'woopack'),
                        'default'                           => '',
                        'show_reset'                        => true,
                    ),
                ),
            ),
        ),
    ),
    'cart_total_style'  => array(
        'title'         => __('Cart Totals', 'woopack'),
        'sections'      => array(
            'cart_total_box_style' 	=> array(
                'title'                 => __('Box', 'woopack'),
                'fields'                => array(
                    'cart_total_bg_color'       => array(
                        'type'                      => 'color',
                        'label'                     => __('Background Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce .cart_totals table.shop_table',
                            'property'                  => 'background-color',
                        ),
                    ),
					'cart_total_border_group'	=> array(
						'type'						=> 'border',
						'label'						=> __('Border Style', 'woopack'),
						'responsive'				=> true,
						'preview'					=> array(
							'type'						=> 'css',
							'selector'					=> '.woocommerce .cart_totals table.shop_table',
						),
					),
                    'cart_total_padding_t_b'    => array(
                        'type'                      => 'unit',
                        'label'                     => __('Top & Bottom Padding', 'woopack'),
						'units'						=> array('px'),
						'slider'					=> true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'rules'                     => array(
                                array(
                                    'selector'          => '.woocommerce .cart_totals table.shop_table',
                                    'property'          => 'padding-top',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => '.woocommerce .cart_totals table.shop_table',
                                    'property'          => 'padding-bottom',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'            => true,
                    ),
                    'cart_total_padding_r_l'    => array(
                        'type'                      => 'unit',
                        'label'                     => __('Right & Left Padding', 'woopack'),
						'units'						=> array('px'),
						'slider'					=> true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'rules' 	                => array(
                                array(
                                    'selector'          => '.woocommerce .cart_totals table.shop_table',
                                    'property'          => 'padding-right',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => '.woocommerce .cart_totals table.shop_table',
                                    'property'          => 'padding-left',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'            => true,
                    ),
                ),
            ),
            'cart_total_padding'    => array(
				'title'                 => __('Cart Totals', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'cart_total_padding'    => array(
                        'type'                  => 'unit',
                        'label' 				=> __('Vertical Spacing', 'woopack'),
						'units'					=> array('px'),
						'slider'				=> true,
                        'preview'               => array(
                            'type'                  => 'css',
                            'rules' 	            => array(
                                array(
                                    'selector'          => '.woocommerce .cart_totals table.shop_table tbody th, .woocommerce .cart_totals table.shop_table tbody td',
                                    'property'          => 'padding-top',
                                    'unit'              => 'px',
                                ),
                                array(
                                    'selector'	        => '.woocommerce .cart_totals table.shop_table tbody th, .woocommerce .cart_totals table.shop_table tbody td',
                                    'property'          => 'padding-bottom',
                                    'unit'              => 'px',
                                ),
                            ),
                        ),
                        'responsive'            => true,
                    ),
                ),
            ),
            'cart_total_separator'  => array(
				'title'                 => __('Separator', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'cart_total_separator_width' => array(
                        'type'                      => 'unit',
                        'label'                     => __('Separator Width', 'woopack'),
						'units'						=> array('px'),
						'slider'					=> true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce .cart_totals table.shop_table tbody th, .woocommerce .cart_totals table.shop_table tbody td',
                            'property'                  => 'border-top-width',
                            'unit'                      => 'px',
                        ),
                        'responsive'                => true,
                    ),
                    'cart_total_separator_color' => array(
                        'type' 				         => 'color',
                        'label' 			         => __('Separator Color', 'woopack'),
                        'show_reset' 		         => true,
                        'show_alpha' 		         => true,
                        'default'                    => '',
                    ),
                ),
            ),
        ),
    ),
    'button_style'      => array(
        'title'             => __('Buttons', 'woopack'),
        'sections'          => array(
            'standard_button'       => array(
                'title'                 => __('Standard Button Colors', 'woopack'),
                'fields'                => array(
                    'standard_button_bg_color'          => array(
                        'type'                              => 'color',
                        'label'                             => __('Background Color', 'woopack'),
                        'default'                           => '',
                        'show_reset'                        => true,
                        'show_alpha'                        => true,
                        'preview'                           => array(
                            'type'                              => 'css',
                            'selector'                          => '.woocommerce button.button, .woocommerce input.button',
                            'property'                          => 'background-color',
                        ),
                    ),
                    'standard_button_bg_color_hover'    => array(
                        'type'                              => 'color',
                        'label'                             => __('Background Hover Color', 'woopack'),
                        'default'                           => '',
                        'show_reset'                        => true,
                        'show_alpha'                        => true,
                    ),
                    'standard_button_color'             => array(
                        'type'                              => 'color',
                        'label'                             => __('Text Color', 'woopack'),
                        'default'                           => '',
                        'show_reset'                        => true,
                    ),
                    'standard_button_color_hover'       => array(
                        'type'                              => 'color',
                        'label'                             => __('Text Color On Hover', 'woopack'),
                        'default'                           => '',
                        'show_reset'                        => true,
                        'show_alpha'                        => true,
                    ),
                ),
            ),
            'standard_button_border'=> array(
				'title'                 => __('Standard Button Border', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
					'standard_button_border_group'	=> array(
						'type'							=> 'border',
						'label'							=> __('Border Style', 'woopack'),
						'responsive'					=> true,
						'preview'						=> array(
							'type'							=> 'css',
							'selector'						=> '.woocommerce button.button,
																.woocommerce input.button',
						),
					),
                    'standard_button_border_color_h'=> array(
                        'type'                          => 'color',
                        'label'                         => __('Border Color on Hover', 'woopack'),
                        'show_reset'                    => true,
                        'show_alpha'                    => true,
                        'default'                       => '',
                    ),
                ),
            ),
            'checkout_button_str'   => array(
				'title'                 => __('Checkout Button Structure', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
					'checkout_padding'		=> array(
						'type'              	=> 'dimension',
						'label' 				=> __('Padding', 'woopack'),
						'slider'				=> true,
						'units'					=> array( 'px' ),
						'responsive' 			=> true,
						'preview' 				=> array(
							'type' 					=> 'css',
							'selector'				=> '.woocommerce .wc-proceed-to-checkout a.button.alt',
							'property'      		=> 'padding',
							'unit'					=> 'px',
						),
					),
                    'checkout_width'        => array(
                        'type'                  => 'select',
                        'label'                 => __('Width', 'woopack'),
                        'default'               => 'auto',
                        'options'               => array(
                            'auto'                  => __('Auto', 'woopack'),
                            'full_width'            => __('Full Width', 'woopack'),
                            'custom'                => __('Custom', 'woopack'),
                        ),
                        'toggle'                => array(
                            'custom'                => array(
                                'fields'                => array('checkout_width_custom', 'checkout_align')
							),
							'auto'					=> array(
								'fields'				=> array('checkout_align')
							)	
                        ),
                    ),
                    'checkout_width_custom' => array(
                        'type'                  => 'unit',
                        'label'                 => __('Custom Width', 'woopack'),
						'units'					=> array('%'),
						'slider'				=> true,
                        'responsive' 			=> true,
                        'preview'				=> array(
                            'type' 					=> 'css',
                            'selector'              => '.woocommerce .wc-proceed-to-checkout a.button.alt',
                            'property'      		=> 'width',
                            'unit'					=> '%',
                        ),
					),
                    'checkout_align'        => array(
                        'type'                  => 'align',
                        'label'                 => __('Alignment', 'woopack'),
                        'default'               => 'center',
                        'preview'               => array(
                            'type'                  => 'css',
                            'selector'              => '.woocommerce .wc-proceed-to-checkout',
                            'property'              => 'text-align',
                        ),
                    ),
                ),
            ),
            'checkout_button_color' => array(
				'title'                 => __('Checkout Button Colors', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'checkout_bg_color'         => array(
                        'type'                      => 'color',
                        'label'                     => __('Background Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce .wc-proceed-to-checkout a.button.alt',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'checkout_bg_color_hover'   => array(
                        'type'                      => 'color',
                        'label'                     => __('Background Hover Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce .wc-proceed-to-checkout a.button.alt:hover',
                            'property'                  => 'background-color',
                        ),
                    ),
                    'checkout_color'            => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce .wc-proceed-to-checkout a.button.alt',
                            'property'                  => 'color',
                        ),
                    ),
                    'checkout_color_hover'      => array(
                        'type'                      => 'color',
                        'label'                     => __('Text Color On Hover', 'woopack'),
                        'default'                   => '',
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'preview'                   => array(
                            'type'                      => 'css',
                            'selector'                  => '.woocommerce .wc-proceed-to-checkout a.button.alt:hover',
                            'property'                  => 'color',
                        ),
                    ),
                ),
            ),
            'checkout_button_border'=> array(
				'title'                 => __('Checkout Button Border', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
					'checkout_border_group'		=> array(
						'type'						=> 'border',
						'label'						=> __('Border Style', 'woopack'),
						'responsive'				=> true,
						'preview'					=> array(
							'type'						=> 'css',
							'selector'					=> '.woocommerce .wc-proceed-to-checkout a.button.alt',
						),
					),
                    'checkout_border_color_h'	=> array(
                        'type'                      => 'color',
                        'label'                     => __('Border Color On Hover', 'woopack'),
                        'show_reset'                => true,
                        'show_alpha'                => true,
                        'default'                   => '',
                    ),
                ),
            ),
        ),
    ),
    'typography'        => array(
        'title'             => __('Typography', 'woopack'),
        'sections' 	        => array(
            'default_font'          => array(
                'title'                 => __('Default', 'woopack'),
                'fields'                => array(
					'default_typography'	=> array(
						'type'        	    	=> 'typography',
						'label'       	    	=> __( 'Typography', 'woopack' ),
						'responsive'  	    	=> true,
						'preview'           	=> array(
							'type'              	=> 'css',
							'selector' 		    	=> '.woocommerce, .cart_totals table.shop_table tbody th, .woocommerce .cart_totals table.shop_table tbody td',
						),
					),
                    'default_color'			=> array(
                        'type'					=> 'color',
                        'label'					=> __('Text Color', 'woopack'),
                        'default'				=> '',
                        'show_reset'			=> true,
                        'preview'				=> array(
                            'type'				=> 'css',
                            'selector'				=> '.woocommerce, .cart_totals table.shop_table tbody th, .woocommerce .cart_totals table.shop_table tbody td',
                            'property'				=> 'color',
                        ),
                    ),
                ),
            ),
            'table_header_font'     => array(
				'title'                 => __('Table Header', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
					'table_header_typography'	=> array(
						'type'        	    		=> 'typography',
						'label'       	    		=> __( 'Typography', 'woopack' ),
						'responsive'  	    		=> true,
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woocommerce table.shop_table.cart thead th',
						),
					),
                    'table_header_color'		=> array(
                        'type'						=> 'color',
                        'label'						=> __('Text Color', 'woopack'),
                        'default'					=> '',
                        'show_reset'				=> true,
                        'preview'					=> array(
                            'type'						=> 'css',
                            'selector'					=> '.woocommerce table.shop_table.cart thead th',
                            'property'					=> 'color',
                        ),
                    ),
                ),
            ),
            'product_name_font'     => array(
				'title'                 => __('Product Name', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
                    'product_name_color'        => array(
                        'type'              => 'color',
                        'label'             => __('Text Color', 'woopack'),
                        'default'           => '',
                        'show_reset'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.woocommerce table.shop_table.cart tbody td.product-name a',
                            'property'          => 'color',
                        ),
                    ),
                    'product_name_color_hover'  => array(
                        'type'              => 'color',
                        'label'             => __('Color on Hover', 'woopack'),
                        'default'           => '',
                        'show_reset'        => true,
                        'preview'           => array(
                            'type'              => 'css',
                            'selector'          => '.woocommerce table.shop_table.cart tbody td.product-name a:hover',
                            'property'          => 'color',
                        ),
                    ),
                ),
            ),
            'cart_totals_title_font'=> array(
				'title'                 => __('Cart Totals Title', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
					'cart_totals_title_typography'	=> array(
						'type'        	    			=> 'typography',
						'label'       	    			=> __( 'Typography', 'woopack' ),
						'responsive'  	    			=> true,
						'preview'           			=> array(
							'type'              			=> 'css',
							'selector' 		    			=> '.woocommerce .cart_totals h2',
						),
					),
                    'cart_totals_title_color'		=> array(
                        'type'							=> 'color',
                        'label'							=> __('Text Color', 'woopack'),
                        'default'						=> '',
                        'show_reset'					=> true,
                        'preview'						=> array(
                            'type'						=> 'css',
                            'selector'						=> '.woocommerce .cart_totals h2',
                            'property'						=> 'color',
                        ),
                    ),
                ),
            ),
            'standard_button_font'  => array(
				'title'                 => __('Standard Button', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
					'standard_button_typography'	=> array(
						'type'        	    			=> 'typography',
						'label'       	    			=> __( 'Typography', 'woopack' ),
						'responsive'  	    			=> true,
						'preview'           			=> array(
							'type'              			=> 'css',
							'selector' 		    			=> '.woocommerce button.button,
																.woocommerce input.button',
						),
					),
                ),
            ),
            'checkout_button_font'  => array(
				'title'                 => __('Checkout Button', 'woopack'),
				'collapsed'				=> true,
                'fields'                => array(
					'checkout_button_typography'	=> array(
						'type'        	    			=> 'typography',
						'label'       	    			=> __( 'Typography', 'woopack' ),
						'responsive'  	    			=> true,
						'preview'           			=> array(
							'type'              			=> 'css',
							'selector' 		    			=> '.woocommerce .wc-proceed-to-checkout a.button.alt',
						),
					)
                ),
            ),
        ),
    ),
));
