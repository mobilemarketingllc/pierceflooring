.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce form {
	<?php WooPack_Helper::print_css( 'background-color', $settings->form_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->form_text_color ); ?>
}

<?php
// Form Border
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'form_border',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce form",
) );

// Form Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'form_padding',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce form",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'form_padding_top',
		'padding-right' 	=> 'form_padding_right',
		'padding-bottom' 	=> 'form_padding_bottom',
		'padding-left' 		=> 'form_padding_left',
	),
) );
?>

<?php
// Form Title Margin Top
FLBuilderCSS::responsive_rule( array(
	'settings'		=> $settings,
	'setting_name'	=> 'form_title_margin_top',
	'selector'		=> ".fl-node-$id .woopack-my-account .woocommerce h2, .fl-node-$id .woopack-my-account .woocommerce h3",
	'prop'			=> 'margin-top',
	'unit'			=> 'px'
) );

// Form Title Margin Bottom
FLBuilderCSS::responsive_rule( array(
	'settings'		=> $settings,
	'setting_name'	=> 'form_title_margin_bottom',
	'selector'		=> ".fl-node-$id .woopack-my-account .woocommerce h2, .fl-node-$id .woopack-my-account .woocommerce h3",
	'prop'			=> 'margin-bottom',
	'unit'			=> 'px'
) );

// Form Title Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'form_title_typography',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce h2, .fl-node-$id .woopack-my-account .woocommerce h3",
) );
?>

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce h2, .fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce h3 {
	<?php WooPack_Helper::print_css( 'color', $settings->form_title_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce form a {
	<?php WooPack_Helper::print_css( 'color', $settings->form_link_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce form a:hover {
	<?php WooPack_Helper::print_css( 'color', $settings->form_link_color_hover ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .form-row label {
	<?php WooPack_Helper::print_css( 'color', $settings->label_color ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->label_margin, 'px' ); ?>
}
<?php

// Label Margin
FLBuilderCSS::responsive_rule( array(
	'settings'		=> $settings,
	'setting_name'	=> 'label_margin',
	'selector'		=> ".fl-node-$id .woopack-my-account .woocommerce .form-row label",
	'prop'			=> 'margin-bottom',
	'unit'			=> 'px'
) );

// Label Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'label_typography',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .form-row label",
) );

// Input Margin
FLBuilderCSS::responsive_rule( array(
	'settings'		=> $settings,
	'setting_name'	=> 'input_margin_bottom',
	'selector'		=> ".fl-node-$id .woopack-my-account .woocommerce .form-row",
	'prop'			=> 'margin-bottom',
	'unit'			=> 'px'
) );

?>

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .form-row .input-text,
.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce form .form-row .select2-container .select2-selection--single {
	<?php WooPack_Helper::print_css( 'background-color', $settings->input_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->input_text_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce form .form-row .select2-container .select2-selection--single .select2-selection__rendered {
	<?php WooPack_Helper::print_css( 'color', $settings->input_text_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .form-row .input-text::-webkit-input-placeholder {
	<?php WooPack_Helper::print_css( 'color', $settings->input_placeholder_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .form-row .input-text:-moz-placeholder {
	<?php WooPack_Helper::print_css( 'color', $settings->input_placeholder_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .form-row .input-text::-moz-placeholder {
	<?php WooPack_Helper::print_css( 'color', $settings->input_placeholder_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .form-row .input-text:-ms-input-placeholder {
	<?php WooPack_Helper::print_css( 'color', $settings->input_placeholder_color ); ?>
}

<?php
// Input Height
FLBuilderCSS::responsive_rule( array(
	'settings'		=> $settings,
	'setting_name'	=> 'input_height',
	'selector'		=> ".fl-node-$id .woopack-my-account .woocommerce .form-row .input-text, .fl-node-$id .woopack-my-account .woocommerce form .form-row .select2-container .select2-selection--single",
	'prop'			=> 'height',
	'unit'			=> 'px'
) );

// Input Border
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'input_border',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce form-row .input-text, .fl-node-$id .woopack-my-account .woocommerce form .form-row .select2-container .select2-selection--single",
) );

// Input Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'input_padding',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce form-row .input-text, .fl-node-$id .woopack-my-account .woocommerce form .form-row .select2-container .select2-selection--single",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'input_padding_top',
		'padding-right' 	=> 'input_padding_right',
		'padding-bottom' 	=> 'input_padding_bottom',
		'padding-left' 		=> 'input_padding_left',
	),
) );

// Input Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'input_typography',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .form-row .input-text, .fl-node-$id .woopack-my-account .woocommerce form .form-row .select2-container .select2-selection--single",
) );

// Input Description Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'input_desc_typography',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .form-row span",
) );
?>

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .form-row span {
	<?php WooPack_Helper::print_css( 'color', $settings->input_desc_color ); ?>
}

<?php
// *********************
// General Buttons
// ********************* 
?>
.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-MyAccount-content .button {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color ); ?>
}

<?php
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'button_border_group',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-content .button,
						.fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-content .button:hover",
) );

FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'button_padding',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-content .button,
						.fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-content .button:hover",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'button_padding_top',
		'padding-right' 	=> 'button_padding_right',
		'padding-bottom' 	=> 'button_padding_bottom',
		'padding-left' 		=> 'button_padding_left',
	),
) );

FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'button_typography',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-content .button",
) );

?>

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-MyAccount-content .button:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color_hover ); ?>
    <?php WooPack_Helper::print_css( 'color', $settings->button_color_hover ); ?>
    <?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color_hover ); ?>
}

<?php
// *********************
// Form Buttons
// ********************* 
?>
.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-form-register .form-row:last-child,
.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-form-login .form-row:last-child,
.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce form .woopack-my-account-button {
	<?php WooPack_Helper::print_css( 'text-align', $settings->form_button_alignment ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce form .form-row button,
.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button {
	<?php WooPack_Helper::print_css( 'width', '100', '%', 'full_width' == $settings->form_button_width ); ?>
	<?php WooPack_Helper::print_css( 'width', $settings->form_button_width_custom, '%', 'custom' == $settings->form_button_width ); ?>
	<?php WooPack_Helper::print_css( 'background-color', $settings->form_button_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->form_button_color ); ?>
}

<?php
FLBuilderCSS::responsive_rule( array(
	'settings'		=> $settings,
	'setting_name'	=> 'form_button_margin_top',
	'selector'		=> ".fl-node-$id .woopack-my-account .woocommerce form .form-row button,
						.fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button",
	'prop'			=> 'margin-top',
	'unit'			=> 'px'
) );

FLBuilderCSS::responsive_rule( array(
	'settings'		=> $settings,
	'setting_name'	=> 'form_button_margin_bottom',
	'selector'		=> ".fl-node-$id .woopack-my-account .woocommerce form .form-row button,
						.fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button",
	'prop'			=> 'margin-bottom',
	'unit'			=> 'px'
) );

FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'form_button_border_group',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce form .form-row button,
						.fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button,
						.fl-node-$id .woopack-my-account .woocommerce form .form-row button:hover,
						.fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button:hover",
) );

FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'form_button_padding',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce form .form-row button,
						.fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button,
						.fl-node-$id .woopack-my-account .woocommerce form .form-row button:hover,
						.fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button:hover",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'form_button_padding_top',
		'padding-right' 	=> 'form_button_padding_right',
		'padding-bottom' 	=> 'form_button_padding_bottom',
		'padding-left' 		=> 'form_button_padding_left',
	),
) );

FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'form_button_typography',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce form .form-row button,
						.fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button",
) );
?>

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce form .form-row button:hover,
.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->form_button_bg_color_hover ); ?>
    <?php WooPack_Helper::print_css( 'color', $settings->form_button_color_hover ); ?>
    <?php WooPack_Helper::print_css( 'border-color', $settings->form_button_border_color_hover ); ?>
}

<?php
// *********************
// Error Messages
// *********************
?>
.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-error {
	<?php WooPack_Helper::print_css( 'background-color', $settings->error_message_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->error_message_text_color ); ?>
}

<?php
// Error Border
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'error_message_border',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .woocommerce-error",
) );

?>

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-error:before {
	<?php WooPack_Helper::print_css( 'color', $settings->error_message_icon_color ); ?>
}

<?php
// *********************
// Messages Info
// *********************
?>
.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-info,
.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-message {
	<?php WooPack_Helper::print_css( 'background-color', $settings->message_info_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->message_info_text_color ); ?>
}

<?php
// Message Border
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'message_info_border',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .woocommerce-info, .fl-node-$id .woopack-my-account .woocommerce .woocommerce-message",
) );

// Message Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'notices_typography',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .woocommerce-error, .fl-node-$id .woopack-my-account .woocommerce .woocommerce-info, .fl-node-$id .woopack-my-account .woocommerce .woocommerce-message",
) );
?>

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-info:before,
.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-message:before {
	<?php WooPack_Helper::print_css( 'color', $settings->message_info_icon_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce table {
	<?php if ( '' !== $settings->rows_border_width ) { ?>
		border-collapse: collapse;
	<?php } ?>
}

<?php
// Table Border
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'table_border',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce table",
) );

// Table Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'rows_typography',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce table tbody tr td, .fl-node-$id .woopack-my-account .woocommerce table thead tr th",
) );

// Header Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'header_padding',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce table thead tr th",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'header_padding_top',
		'padding-right' 	=> 'header_padding_right',
		'padding-bottom' 	=> 'header_padding_bottom',
		'padding-left' 		=> 'header_padding_left',
	),
) );
// Rows Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'rows_padding',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce table tbody tr td",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'rows_padding_top',
		'padding-right' 	=> 'rows_padding_right',
		'padding-bottom' 	=> 'rows_padding_bottom',
		'padding-left' 		=> 'rows_padding_left',
	),
) );

// Header Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'header_typography',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce table thead tr th",
) );
?>

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce table tr {
	<?php if ( $settings->rows_border_width !== '' ) { ?>
	border: 0 !important;
	<?php } ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce table thead tr th,
.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce table tbody tr td {
	vertical-align: <?php echo $settings->rows_vertical_alignment; ?>;
	<?php if ( $settings->rows_border_width !== '' ) { ?>
		border: 0 !important;
		<?php if ( $settings->cells_border === 'horizontal' ) { ?>
			border-width: <?php echo $settings->rows_border_width; ?>px 0 0 0 !important;
		<?php } elseif ( $settings->cells_border === 'vertical' ) { ?>
			border-width: 0 0 0 <?php echo $settings->rows_border_width; ?>px !important;
		<?php } else { ?>
			border-width: <?php echo $settings->rows_border_width; ?>px !important;
		<?php } ?>
		border-style: solid !important;
		<?php WooPack_Helper::print_css( 'border-color', $settings->rows_border_color, ' !important' ); ?>
	<?php } ?>
}

<?php if ( $settings->cells_border === 'horizontal' ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce table thead tr th {
		border-top: 0 !important;
	}
<?php } ?>
<?php if ( $settings->cells_border === 'vertical' ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce table thead tr th:first-child,
	.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce table tbody tr td:first-child {
		border-left: 0 !important;
	}
<?php } ?>

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce table thead th {
    <?php WooPack_Helper::print_css( 'background-color', $settings->header_background ); ?>;
	<?php WooPack_Helper::print_css( 'color', $settings->header_color ); ?>;
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce table tbody tr td {
	<?php WooPack_Helper::print_css( 'background-color', $settings->rows_background ); ?>;
	<?php WooPack_Helper::print_css( 'color', $settings->rows_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce table tbody tr:nth-child(even) td {
	<?php WooPack_Helper::print_css( 'background-color', $settings->even_rows_background ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->even_rows_color ); ?>
}

<?php
// Header Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'header_padding',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .woocommerce-Addresses .woocommerce-Address-title",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'header_padding_top',
		'padding-right' 	=> 'header_padding_right',
		'padding-bottom' 	=> 'header_padding_bottom',
		'padding-left' 		=> 'header_padding_left',
	),
) );
// Rows Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'rows_padding',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-content address",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'rows_padding_top',
		'padding-right' 	=> 'rows_padding_right',
		'padding-bottom' 	=> 'rows_padding_bottom',
		'padding-left' 		=> 'rows_padding_left',
	),
) );

// Tabs alignment top position
FLBuilderCSS::responsive_rule( array(
	'settings'		=> $settings,
	'setting_name'	=> 'tabs_alignment',
	'selector'		=> ".fl-node-$id .woopack-my-account.tabs-position-top .woocommerce .woocommerce-MyAccount-navigation ul,
						.fl-node-$id .woopack-my-account.tabs-position-md-top .woocommerce .woocommerce-MyAccount-navigation ul,
						.fl-node-$id .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-navigation ul",
	'prop'			=> 'align-items',
) );

FLBuilderCSS::responsive_rule( array(
	'settings'		=> $settings,
	'setting_name'	=> 'tabs_alignment',
	'selector'		=> ".fl-node-$id .woopack-my-account.tabs-position-top .woocommerce .woocommerce-MyAccount-navigation ul,
						.fl-node-$id .woopack-my-account.tabs-position-md-top .woocommerce .woocommerce-MyAccount-navigation ul,
						.fl-node-$id .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-navigation ul",
	'prop'			=> 'justify-content',
) );
?>


<?php // Tabs Navigation ?>
.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li a {
	<?php WooPack_Helper::print_css( 'background-color', $settings->tab_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->tab_text_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li.is-active a,
.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li:hover a {
	<?php WooPack_Helper::print_css( 'background-color', $settings->active_tab_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->active_tab_text_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-left .woocommerce .woocommerce-MyAccount-navigation ul li,
.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-right .woocommerce .woocommerce-MyAccount-navigation ul li {
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->tabs_margin . 'px' ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-top .woocommerce .woocommerce-MyAccount-navigation ul li {
	<?php WooPack_Helper::print_css( 'margin-right', $settings->tabs_margin . 'px' ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-left .woocommerce .woocommerce-MyAccount-navigation ul li:last-child,
.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-right .woocommerce .woocommerce-MyAccount-navigation ul li:last-child {
	margin-bottom: 0;
}

.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-top .woocommerce .woocommerce-MyAccount-navigation ul li:last-child {
	margin-right: 0;
}

<?php

// Tabs Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'tab_padding',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li a",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'tab_padding_top',
		'padding-right' 	=> 'tab_padding_right',
		'padding-bottom' 	=> 'tab_padding_bottom',
		'padding-left' 		=> 'tab_padding_left',
	),
) );

// Tab Border
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'tab_border',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li a",
) );

// Active Tab Border
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'active_tab_border',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li.is-active a,
						.fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li:hover a",
) );

// Tabs Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'tabs_typography',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li a",
) );

// Tab Content Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'tab_content_typography',
	'selector' 		=> ".fl-node-$id .woopack-my-account .woocommerce .woocommerce-MyAccount-content",
) );
?>

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-MyAccount-content {
	<?php WooPack_Helper::print_css( 'color', $settings->tab_content_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-MyAccount-content a {
	<?php WooPack_Helper::print_css( 'color', $settings->tab_content_link_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-my-account .woocommerce .woocommerce-MyAccount-content a:hover {
	<?php WooPack_Helper::print_css( 'color', $settings->tab_content_link_hover ); ?>
}

@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-top .woocommerce .woocommerce-MyAccount-navigation,
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-top .woocommerce .woocommerce-MyAccount-content {
		width: 100%;
		float: none;
		clear: both;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-top .woocommerce .woocommerce-MyAccount-navigation {
		margin-bottom: 30px;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-top .woocommerce .woocommerce-MyAccount-navigation ul li {
		display: inline-block;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-left .woocommerce .woocommerce-MyAccount-navigation ul,
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-right .woocommerce .woocommerce-MyAccount-navigation ul,
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-left .woocommerce .woocommerce-MyAccount-navigation ul li,
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-right .woocommerce .woocommerce-MyAccount-navigation ul li {
		display: block;
	}
	
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-left .woocommerce .woocommerce-MyAccount-navigation {
		float: left;
		width: 30%;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-left .woocommerce .woocommerce-MyAccount-content {
		float: right;
		width: 68%;
		clear: none;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-right .woocommerce .woocommerce-MyAccount-navigation {
		float: right;
		width: 30%;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-right .woocommerce .woocommerce-MyAccount-content {
		float: left;
		width: 68%;
		clear: none;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-top .woocommerce .woocommerce-MyAccount-navigation ul {
		display: flex;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-left .woocommerce .woocommerce-MyAccount-navigation ul li,
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-right .woocommerce .woocommerce-MyAccount-navigation ul li {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->tabs_margin_medium . 'px' ); ?>
		margin-right: 0;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-top .woocommerce .woocommerce-MyAccount-navigation ul li {
		<?php WooPack_Helper::print_css( 'margin-right', $settings->tabs_margin_medium . 'px' ); ?>
		margin-bottom: 0;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-left .woocommerce .woocommerce-MyAccount-navigation ul li:last-child,
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-right .woocommerce .woocommerce-MyAccount-navigation ul li:last-child {
		margin-bottom: 0;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-md-top .woocommerce .woocommerce-MyAccount-navigation ul li:last-child {
		margin-right: 0;
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-navigation,
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-content {
		width: 100%;
		float: none;
		clear: both;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-navigation {
		margin-bottom: 30px;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-navigation ul {
		flex-direction: column;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-navigation ul li {
		display: none;
		position: relative;
	}
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-navigation.expanded ul li,
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-navigation ul li.is-active {
		display: block;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-navigation ul li a:after {
		position: absolute;
		right: 0px;
		top: 0px;
		z-index: 3;
		cursor: pointer;
		height: 100%;
		width: 50px;
		text-align: center;
		display: flex;
		align-items: center;
		justify-content: center;
	}
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-navigation:not(.expanded) ul li.is-active a:after {
		content: "+";
	}
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-navigation.expanded ul li:first-child a:after {
		content: "-";
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-left .woocommerce .woocommerce-MyAccount-navigation ul,
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-right .woocommerce .woocommerce-MyAccount-navigation ul,
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-left .woocommerce .woocommerce-MyAccount-navigation ul li,
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-right .woocommerce .woocommerce-MyAccount-navigation ul li {
		display: block;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-left .woocommerce .woocommerce-MyAccount-navigation {
		float: left;
		width: 30%;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-left .woocommerce .woocommerce-MyAccount-content {
		float: right;
		width: 68%;
		clear: none;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-right .woocommerce .woocommerce-MyAccount-navigation {
		float: right;
		width: 30%;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-right .woocommerce .woocommerce-MyAccount-content {
		float: left;
		width: 68%;
		clear: none;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-navigation ul {
		display: block;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-left .woocommerce .woocommerce-MyAccount-navigation ul li,
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-right .woocommerce .woocommerce-MyAccount-navigation ul li {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->tabs_margin_responsive . 'px' ); ?>
		margin-right: 0;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-navigation ul li {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->tabs_margin_responsive . 'px' ); ?>
		margin-right: 0;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-left .woocommerce .woocommerce-MyAccount-navigation ul li:last-child,
	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-right .woocommerce .woocommerce-MyAccount-navigation ul li:last-child {
		margin-bottom: 0;
	}

	.fl-node-<?php echo $id; ?> .woopack-my-account.tabs-position-sm-top .woocommerce .woocommerce-MyAccount-navigation ul li:last-child {
		margin-right: 0;
	}
}
