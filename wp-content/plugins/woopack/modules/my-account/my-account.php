<?php
/**
* @class WooPackMyAccount
*/
class WooPackMyAccount extends FLBuilderModule {
	/**
	* Constructor function for the module. You must pass the
	* name, description, dir and url in an array to the parent class.
	*
	* @method __construct
	*/
	public function __construct()
	{
		parent::__construct( array(
			'name' 				=> __( 'My Account', 'woopack' ),
			'description' 		=> __( 'Addon to display My Account.', 'woopack' ),
			'group'             => WooPack_Helper::get_modules_group(),
			'category' 			=> WOOPACK_CAT,
			'dir' 				=> WOOPACK_DIR . 'modules/my-account/',
			'url' 				=> WOOPACK_URL . 'modules/my-account/',
			'editor_export' 	=> true, // Defaults to true and can be omitted.
			'enabled' 			=> true, // Defaults to true and can be omitted.
		) );
	}

	/**
	* CUstomize My Account tabs.
	*
	* @since 1.3.8
	*/
	public function customize_my_account_tabs( $menu_links ) {

		$settings = $this->settings;

		if ( isset( $settings->show_dashboard ) && 'no' === $settings->show_dashboard ) {
			unset( $menu_links['dashboard'] ); // Remove Dashboard tab
		}
		if ( isset( $settings->show_orders ) && 'no' === $settings->show_orders ) {
			unset( $menu_links['orders'] ); // Remove Orders tab
		}
		if ( isset( $settings->show_downloads ) && 'no' === $settings->show_downloads ) {
			unset( $menu_links['downloads'] ); // Remove Downloads tab
		}
		if ( isset( $settings->show_addresses ) && 'no' === $settings->show_addresses ) {
			unset( $menu_links['edit-address'] ); // Removed Addresses tab
		}
		if ( isset( $settings->show_account_details ) && 'no' === $settings->show_account_details ) {
			unset( $menu_links['edit-account'] ); // Remove Account details tab
		}
		if ( isset( $settings->show_logout_link ) && 'no' === $settings->show_logout_link ) {
			unset( $menu_links['customer-logout'] ); // Remove Logout Link
		}

		return $menu_links;
	}

	public static function get_endpoints() {
		$endpoints = array(
			''				=> __( 'dashboard', 'woopack' ),
			'orders'		=> 'orders',
			//'view-order'	=> 'view-order',
			'downloads'		=> 'downloads',
			'edit-address' 	=> 'edit-address',
			//'payment-methods'	=> 'payment-methods',
			//'add-payment-method'	=> 'add-payment-method',
			'edit-account'	=> 'edit-account'
		);

		return $endpoints;
	}

	/**
	 * Get module notice.
	 *
	 * @since 1.3.8
	 *
	 * @return string
	 */
	static public function get_module_notice() {
		$notice = '';
		$notice .= sprintf(
			__( 'Please change the default my-account page in the WooCommerce\'s Settings to the current page so that the module works properly and you can read the document <a href="%s" target="_blank">here</a>.', 'woopack' ),
			'https://wpbeaveraddons.com/docs/woopack/modules/my-account/set-my-account-page'
		);

		return $notice;
	}
}
/**
* Register the module and its form settings.
*/
FLBuilder::register_module( 'WooPackMyAccount', array(
	'general' => array(
		'title'	=> __( 'General', 'woopack' ),
		'sections' => array(
			'preview'	=> array(
				'title'		=> __( 'Preview', 'woopack' ),
				'fields'	=> array(
					'endpoint'	=> array(
						'type'		=> 'select',
						'label'		=> __( 'Select Endpoint', 'woopack' ),
						'options'	=> WooPackMyAccount::get_endpoints()
					),
				),
			),
			'general' => array(
				'title'	=> __( 'Tabs', 'woopack' ),
				'fields' => array(
					'show_dashboard' 	=> array(
						'type'          => 'select',
						'label'         => __( 'Show Dashboard?', 'woopack' ),
						'default'       => 'yes',
						'options'       => array(
							'yes'           => __( 'Yes', 'woopack' ),
							'no'            => __( 'No', 'woopack' ),
						),
					),
					'show_orders' 	=> array(
						'type'          => 'select',
						'label'         => __( 'Show Orders?', 'woopack' ),
						'default'       => 'yes',
						'options'       => array(
							'yes'           => __( 'Yes', 'woopack' ),
							'no'            => __( 'No', 'woopack' ),
						),
					),
					'show_downloads' 	=> array(
						'type'          => 'select',
						'label'         => __( 'Show Downloads?', 'woopack' ),
						'default'       => 'yes',
						'options'       => array(
							'yes'           => __( 'Yes', 'woopack' ),
							'no'            => __( 'No', 'woopack' ),
						),
					),
					'show_addresses' 	=> array(
						'type'          => 'select',
						'label'         => __( 'Show Addresses?', 'woopack' ),
						'default'       => 'yes',
						'options'       => array(
							'yes'           => __( 'Yes', 'woopack' ),
							'no'            => __( 'No', 'woopack' ),
						),
					),
					'show_account_details' => array(
						'type'          => 'select',
						'label'         => __( 'Show Account Details?', 'woopack' ),
						'default'       => 'yes',
						'options'       => array(
							'yes' => __( 'Yes', 'woopack' ),
							'no' => __( 'No', 'woopack' ),
						),
					),
					'show_logout_link' 	=> array(
						'type'          => 'select',
						'label'         => __( 'Show Logout Link?', 'woopack' ),
						'default'       => 'yes',
						'options'       => array(
							'yes'           => __( 'Yes', 'woopack' ),
							'no'            => __( 'No', 'woopack' ),
						),
					),
				),
			),
		),
	),
	'tabs_style'    => array(
		'title'         => __( 'Tabs', 'woopack' ),
		'description'	=> __( 'Controls styling for tabs and tab content\'s links and text.', 'woopack' ),
		'sections'      => array(
			'tabs_normal'         => array(
				'title'                 => __( 'Default', 'woopack' ),
				'fields'                => array(
					'tabs_position'  => array(
						'type'    => 'select',
						'label'   => __( 'Position', 'woopack' ),
						'default' => 'left',
						'options' => array(
							'left'   => __( 'Left', 'woopack' ),
							'top'   => __( 'Top', 'woopack' ),
							'right'   => __( 'Right', 'woopack' ),
						),
						'responsive'            => array(
							'placeholder' 		    => array(
								'default'           => 'left',
								'medium' 			=> 'left',
								'responsive'        => 'left',
							),
						),
						'toggle'	=> array(
							'top'	=> array(
								'fields' => array( 'tabs_alignment' ),
							),
						),
					),
					'tabs_alignment'		=> array(
						'type'              	=> 'align',
						'label'             	=> __( 'Alignment', 'woopack' ),
						'default'           	=> 'left',
						'preview'           	=> array(
							'type'              	=> 'css',
							'selector'          	=> '.woopack-my-account.tabs-position-top .woocommerce .woocommerce-MyAccount-navigation ul',
							'property'          	=> 'text-align',
						),
					),
					'tab_bg_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Background Color', 'woopack' ),
						'show_reset'                    => true,
						'show_alpha'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li a',
							'property'					=> 'background-color'
						),
					),
					'tab_text_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Text Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li a',
							'property'					=> 'color'
						),
					),
					'tabs_margin'    	=> array(
						'type' 							=> 'unit',
						'label' 						=> __( 'Spacing', 'woopack' ),
						'default' 						=> '',
						'units'							=> array( 'px' ),
						'slider'						=> true,
						'responsive' 					=> true,
						'preview'           			=> array(
							'type'              			=> 'css',
							'rules'							=> array(
								array(
									'selector'          			=> '.woopack-my-account.tabs-position-left .woocommerce .woocommerce-MyAccount-navigation ul li, .woopack-my-account.tabs-position-right .woocommerce .woocommerce-MyAccount-navigation ul li',
									'property'          			=> 'margin-bottom',
									'unit'							=> 'px'
								),
								array(
									'selector'          			=> '.woopack-my-account.tabs-position-top .woocommerce .woocommerce-MyAccount-navigation ul li, .woopack-my-account.tabs-position-right .woocommerce .woocommerce-MyAccount-navigation ul li',
									'property'          			=> 'margin-right',
									'unit'							=> 'px'
								)
							)
						),
					),
					'tab_padding'		=> array(
						'type'				=> 'dimension',
						'label'				=> __( 'Padding', 'woopack' ),
						'default'			=> '',
						'units'				=> array( 'px' ),
						'slider'			=> true,
						'responsive'		=> true,
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li a',
							'property'					=> 'padding'
						),
					),
					'tab_border'		=> array(
						'type'						=> 'border',
						'label'						=> __( 'Border', 'woopack' ),
						'responsive'				=> true,
						'preview'					=> array(
							'type'						=> 'css',
							'selector'					=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li a',
						),
					),
					'tabs_typography'		=> array(
						'type'        	    		=> 'typography',
						'label'       	    		=> __( 'Typography', 'woopack' ),
						'responsive'  	    		=> true,
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li a',
						),
					),
				),
			),
			'tabs_active'         => array(
				'title'                 => __( 'Active Tab', 'woopack' ),
				'collapsed'	=> true,
				'fields'                => array(
					'active_tab_bg_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Background Color', 'woopack' ),
						'show_reset'                    => true,
						'show_alpha'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li.is-active a, .woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li:hover a',
							'property'					=> 'background-color'
						),
					),
					'active_tab_text_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Text Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li.is-active a, .woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li:hover a',
							'property'					=> 'color'
						),
					),
					'active_tab_border'		=> array(
						'type'						=> 'border',
						'label'						=> __( 'Border', 'woopack' ),
						'responsive'				=> true,
						'preview'					=> array(
							'type'						=> 'css',
							'selector'					=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li.is-active a, .woopack-my-account .woocommerce .woocommerce-MyAccount-navigation ul li:hover a',
						),
					),
				),
			),
			'tab_content'	=> array(
				'title' 	=> __( 'Tab Content', 'woopack' ),
				'collapsed'	=> true,
				'fields' 	=> array(
					'tab_content_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Text Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-content',
							'property'					=> 'color'
						),
					),
					'tab_content_link_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Link Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-content a',
							'property'					=> 'color'
						),
					),
					'tab_content_link_hover'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Link Hover Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-content a:hover',
							'property'					=> 'color'
						),
					),
					'tab_content_typography'		=> array(
						'type'        	    		=> 'typography',
						'label'       	    		=> __( 'Typography', 'woopack' ),
						'responsive'  	    		=> true,
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-content',
						),
					),
				),
			),
		),
	),
	'tables_style'			=> array(
		'title'	=> __( 'Tables', 'woopack' ),
		'description'	=> __( 'Controls styling for all tables.', 'woopack' ),
		'sections'	=> array(
			'table_style'	=> array(
				'title'	=> __( 'Table', 'woopack' ),
				'fields'	=> array(
					'table_border'	=> array(
						'type'					=> 'border',
						'label'					=> __( 'Border', 'woopack' ),
						'responsive'			=> true,
						'preview'				=> array(
							'type'					=> 'css',
							'selector'				=> '.woopack-my-account .woocommerce table',
						),
					),
					'rows_typography'		=> array(
						'type'        	    		=> 'typography',
						'label'       	    		=> __( 'Typography', 'woopack' ),
						'responsive'  	    		=> true,
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce table tbody tr td, .woopack-my-account .woocommerce table thead tr th',
						),
					),
				),
			),
			'header_style'	=> array(
				'title'	=> __( 'Header', 'woopack' ),
				'collapsed'	=> true,
				'fields'	=> array(
					'header_background'			=> array(
						'type'          => 'color',
						'default'       => '',
						'label'         => __( 'Background Color', 'woopack' ),
						'show_reset'	=> true,
						'show_alpha'	=> true,
						'connections'	=> array( 'color' ),
						'preview'	=> array(
							'type'		=> 'css',
							'selector'	=> '.woopack-my-account .woocommerce table thead',
							'property'	=> 'background-color'
						),
					),
					'header_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Text Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce table thead th',
							'property'					=> 'color'
						),
					),
					'header_padding'	=> array(
						'type'				=> 'dimension',
						'label'				=> __( 'Padding', 'woopack' ),
						'slider'			=> true,
						'units'				=> array( 'px' ),
						'preview'			=> array(
							'type'				=> 'css',
							'selector'			=> '.woopack-my-account .woocommerce table thead tr th',
							'property'			=> 'padding',
							'unit'				=> 'px',
						),
						'responsive'		=> true,
					),
					'header_typography'		=> array(
						'type'        	    		=> 'typography',
						'label'       	    		=> __( 'Typography', 'woopack' ),
						'responsive'  	    		=> true,
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce table thead tr th',
						),
					),
				),
			),
			'row_style'	=> array(
				'title'		=> __( 'Rows', 'woopack' ),
				'collapsed'	=> true,
				'fields'	=> array(
					'rows_background'     => array(
						'type'          => 'color',
						'default'       => '',
						'label'         => __( 'Background Color', 'woopack' ),
						'show_reset'	=> true,
						'show_alpha'	=> true,
						'connections'	=> array( 'color' ),
						'preview'	=> array(
							'type'		=> 'css',
							'selector'	=> '.woopack-my-account .woocommerce table tbody tr td',
							'property'	=> 'background-color',
						),
					),
					'rows_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Text Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce table tbody tr td',
							'property'					=> 'color',
						),
					),
					'even_rows_background'     => array(
						'type'          => 'color',
						'default'       => '',
						'label'         => __( 'Even Rows Background Color', 'woopack' ),
						'show_reset'	=> true,
						'show_alpha'	=> true,
						'connections'	=> array( 'color' ),
						'preview'	=> array(
							'type'		=> 'css',
							'selector'	=> '.woopack-my-account .woocommerce table tbody tr:nth-child(even) td',
							'property'	=> 'background-color',
						),
					),
					'even_rows_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Even Rows Text Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce table tbody tr:nth-child(even) td',
							'property'					=> 'color',
						),
					),
					'rows_vertical_alignment' => array(
						'type'		=> 'select',
						'label'		=> __( 'Vertical Alignment', 'woopack' ),
						'default'	=> 'middle',
						'options'       => array(
							'top'          => __( 'Top', 'woopack' ),
							'middle'         => __( 'Center', 'woopack' ),
							'bottom'         => __( 'Bottom', 'woopack' ),
						),
						'preview'	=> array(
							'type'		=> 'css',
							'selector'	=> '.woopack-my-account .woocommerce table tbody tr td',
							'property'	=> 'vertical-align'
						),
					),
				),
			),
			'cells_style'	=> array(
				'title'		=> __( 'Cell', 'woopack' ),
				'collapsed'	=> true,
				'fields'	=> array(
					'cells_border' => array(
						'type'		=> 'select',
						'label'		=> __( 'Border', 'woopack' ),
						'default'	=> 'default',
						'options'       => array(
							'default'          	=> __( 'Default', 'woopack' ),
							'horizontal'        => __( 'Horizontal', 'woopack' ),
							'vertical'         	=> __( 'Vertical', 'woopack' ),
						),
					),
					'rows_border_width'    	=> array(
						'type' 							=> 'unit',
						'label' 						=> __( 'Border Width', 'woopack' ),
						'default' 						=> '',
						'units'							=> array( 'px' ),
						'slider'						=> true,
					),
					'rows_border_color'     => array(
						'type'          => 'color',
						'default'       => '',
						'label'         => __( 'Border Color', 'woopack' ),
						'show_reset'	=> true,
						'connections'	=> array( 'color' ),
						'preview'	=> array(
							'type'		=> 'css',
							'rules'		=> array(
								array(
									'selector'	=> '.woopack-my-account .woocommerce table tbody',
									'property'	=> 'border-top-color'
								),
								array(
									'selector'	=> '.woopack-my-account .woocommerce table tbody tr',
									'property'	=> 'border-bottom-color'
								),
								array(
									'selector'	=> '.woopack-my-account .woocommerce table tbody, .woopack-my-account .woocommerce table tbody tr td',
									'property'	=> 'border-left-color'
								),
								array(
									'selector'	=> '.woopack-my-account .woocommerce table tbody',
									'property'	=> 'border-right-color'
								),
							),
						),
					),
					'rows_padding'	=> array(
						'type'				=> 'dimension',
						'label'				=> __( 'Padding', 'woopack' ),
						'slider'			=> true,
						'units'				=> array( 'px' ),
						'preview'			=> array(
							'type'				=> 'css',
							'selector'			=> '.woopack-my-account .woocommerce table tbody tr td',
							'property'			=> 'padding',
							'unit'				=> 'px'
						),
						'responsive'		=> true,
					),
				),
			),
		),
	),
	'button'        => array(
		'title'         => __( 'Buttons', 'woopack' ),
		'description'	=> __( 'Controls styling for buttons.', 'woopack' ),
		'sections'      => array(
			'button_color'      => array(
				'title'             => __('Colors', 'woopack'),
				'collapsed'			=> true,
				'fields'            => array(
					'button_bg_color'       => array(
						'type'              	=> 'color',
						'label'             	=> __('Background Color', 'woopack'),
						'show_reset'        	=> true,
						'show_alpha'        	=> true,
						'preview'           	=> array(
							'type' 					=> 'css',
							'selector'				=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-content .button',
							'property'				=> 'background-color',
						),
					),
					'button_bg_color_hover'	=> array(
						'type'              	=> 'color',
						'label'             	=> __('Background Hover Color', 'woopack'),
						'show_reset'        	=> true,
						'show_alpha'        	=> true,
					),
					'button_color'          => array(
						'type'              	=> 'color',
						'label'             	=> __('Text Color', 'woopack'),
						'show_reset'        	=> true,
						'preview'           	=> array(
							'type'              	=> 'css',
							'selector'          	=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-content .button',
							'property'          	=> 'color',
						),
					),
					'button_color_hover'    => array(
						'type'              	=> 'color',
						'label'             	=> __('Text Hover Color', 'woopack'),
						'show_reset'        	=> true,
					),
				),
			),
			'button_border'     => array(
				'title'             => __('Border', 'woopack'),
				'collapsed'			=> true,
				'fields'            => array(
					'button_border_group'       => array(
						'type'                      => 'border',
						'label'                     => __('Border Style', 'woopack'),
						'responsive'                => true,
						'preview'                   => array(
							'type'                      => 'css',
							'selector' 		            => '.woopack-my-account .woocommerce .woocommerce-MyAccount-content .button',
						),
					),
					'button_border_color_hover'	=> array(
						'type' 						=> 'color',
						'label' 					=> __('Border Hover Color', 'woopack'),
						'show_reset' 				=> true,
					),
				),
			),
			'button_padding'    => array(
				'title'             => __('Padding', 'woopack'),
				'collapsed'			=> true,
				'fields'            => array(
					'button_padding'    => array(
						'type'              	=> 'dimension',
						'label' 				=> __('Padding', 'woopack'),
						'units'					=> array('px'),
						'slider'				=> true,
						'preview' 				=> array(
							'type' 					=> 'css',
							'selector'				=> '.woopack-my-account .woocommerce .woocommerce-MyAccount-content .button',
							'property'				=> 'padding',
							'unit' 					=> 'px'
						),
						'responsive' 			=> true,
					),
				),
			),
			'button_font'       => array(
				'title'             => __( 'Typography', 'woopack' ),
				'collapsed'			=> true,
				'fields'            => array(
					'button_typography' => array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Button Typography', 'woopack' ),
						'responsive'  	    => true,
						'preview'           => array(
							'type'              => 'css',
							'selector' 		    => '.woopack-my-account .woocommerce .woocommerce-MyAccount-content .button',
						),
					),
				),
			),
		),
	),
	'forms_style'       => array(
		'title'         => __( 'Forms', 'woopack' ),
		'description'	=> __( 'Controls styling for addresses, account details section, login and registration form.', 'woopack' ),
		'sections'      => array(
			'form_style'        => array(
				'title'             => __( 'Forms', 'woopack' ),
				'fields'            => array(
					'form_bg_color'     => array(
						'type'              => 'color',
						'label'             => __( 'Background Color', 'woopack' ),
						'default'           => '',
						'show_reset'        => true,
						'show_alpha'        => true,
						'connections'		=> array( 'color' ),
						'preview'					=> array(
							'type'						=> 'css',
							'selector'					=> '.woopack-my-account .woocommerce form',
							'property'					=> 'background-color'
						),
					),
					'form_text_color'     => array(
						'type'              => 'color',
						'label'             => __( 'Text Color', 'woopack' ),
						'default'           => '',
						'show_reset'        => true,
						'connections'		=> array( 'color' ),
						'preview'					=> array(
							'type'						=> 'css',
							'selector'					=> '.woopack-my-account .woocommerce form',
							'property'					=> 'color'
						),
					),
					'form_link_color'     => array(
						'type'              => 'color',
						'label'             => __( 'Link Color', 'woopack' ),
						'default'           => '',
						'show_reset'        => true,
						'connections'		=> array( 'color' ),
						'preview'					=> array(
							'type'						=> 'css',
							'selector'					=> '.woopack-my-account .woocommerce form a',
							'property'					=> 'color'
						),
					),
					'form_link_color_hover'     => array(
						'type'              => 'color',
						'label'             => __( 'Link Hover Color', 'woopack' ),
						'default'           => '',
						'show_reset'        => true,
						'connections'		=> array( 'color' ),
						'preview'					=> array(
							'type'						=> 'css',
							'selector'					=> '.woopack-my-account .woocommerce form a:hover',
							'property'					=> 'color'
						),
					),
					'form_border'		=> array(
						'type'						=> 'border',
						'label'						=> __( 'Border', 'woopack' ),
						'responsive'				=> true,
						'preview'					=> array(
							'type'						=> 'css',
							'selector'					=> '.woopack-my-account .woocommerce form',
						),
					),
					'form_padding'		=> array(
						'type'				=> 'dimension',
						'label'				=> __( 'Padding', 'woopack' ),
						'default'			=> '',
						'units'				=> array( 'px' ),
						'slider'			=> true,
						'responsive'		=> true,
					),
				),
			),
			'heading_style'	=> array(
				'title'	=> __( 'Headings', 'woopack' ),
				'collapsed'	=> true,
				'fields' => array(
					'form_title_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Text Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce h2, .woopack-my-account .woocommerce h3',
							'property'					=> 'color'
						),
					),
					'form_title_margin_top'    	=> array(
						'type' 							=> 'unit',
						'label' 						=> __( 'Margin Top', 'woopack' ),
						'default' 						=> '',
						'units'							=> array( 'px' ),
						'slider'						=> true,
						'responsive' 					=> true,
						'preview'           			=> array(
							'type'              			=> 'css',
							'selector'          			=> '.woopack-my-account .woocommerce h2, .woopack-my-account .woocommerce h3',
							'property'          			=> 'margin-top',
							'unit'							=> 'px'
						),
					),
					'form_title_margin_bottom'	=> array(
						'type' 							=> 'unit',
						'label' 						=> __( 'Margin Bottom', 'woopack' ),
						'default' 						=> '',
						'units'							=> array( 'px' ),
						'slider'						=> true,
						'responsive' 					=> true,
						'preview'           			=> array(
							'type'              			=> 'css',
							'selector'          			=> '.woopack-my-account .woocommerce h2, .woopack-my-account .woocommerce h3',
							'property'          			=> 'margin-bottom',
							'unit'							=> 'px'
						),
					),
					'form_title_typography'		=> array(
						'type'        	    		=> 'typography',
						'label'       	    		=> __( 'Typography', 'woopack' ),
						'responsive'  	    		=> true,
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce h2, .woopack-my-account .woocommerce h3',
						),
					),
				),
			),
			'inputs_style'        => array(
				'title'             => __( 'Inputs', 'woopack' ),
				'collapsed'	=> true,
				'fields'            => array(
					'input_height'      => array(
						'type'                  => 'unit',
						'label'                 => __( 'Height', 'woopack' ),
						'default'               => '',
						'units'				=> array( 'px' ),
						'responsive'			=> true,
					),
					'input_margin_bottom'  	=> array(
						'type'                  	=> 'unit',
						'label'                 	=> __( 'Margin Bottom', 'woopack' ),
						'units'				=> array( 'px' ),
						'responsive'			=> true,
					),
					'input_border'		=> array(
						'type'						=> 'border',
						'label'						=> __( 'Border', 'woopack' ),
						'responsive'				=> true,
						'preview'					=> array(
							'type'						=> 'css',
							'selector'					=> '.woopack-my-account .woocommerce .form-row .input-text',
						),
					),
					'input_padding'		=> array(
						'type'				=> 'dimension',
						'label'				=> __( 'Padding', 'woopack' ),
						'default'			=> '',
						'units'				=> array( 'px' ),
						'slider'			=> true,
						'responsive'			=> true,
					),
					'input_bg_color'    => array(
						'type'              => 'color',
						'label'             => __( 'Background Color', 'woopack' ),
						'show_reset'        => true,
						'show_alpha'        => true,
						'connections'		=> array( 'color' ),
						'preview'					=> array(
							'type'						=> 'css',
							'selector'					=> '.woopack-my-account .woocommerce .form-row .input-text, .woopack-my-account .woocommerce form .form-row .select2-container .select2-selection--single',
							'property'					=> 'background-color'
						),
					),
					'input_text_color'  => array(
						'type'              => 'color',
						'label'             => __( 'Text Color', 'woopack' ),
						'show_reset'        => true,
						'connections'		=> array( 'color' ),
						'preview'					=> array(
							'type'						=> 'css',
							'selector'					=> '.woopack-my-account .woocommerce .form-row .input-text, .woopack-my-account .woocommerce form .form-row .select2-container .select2-selection--single .select2-selection__rendered',
							'property'					=> 'color'
						),
					),
					'input_desc_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Description Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .form-row span',
							'property'					=> 'color'
						),
					),
					'input_placeholder_color'  => array(
						'type'              => 'color',
						'label'             => __( 'Placeholder Color', 'woopack' ),
						'show_reset'        => true,
						'connections'		=> array( 'color' ),
						'preview'					=> array(
							'type'						=> 'css',
							'selector'					=> '.woopack-my-account .woocommerce .form-row .input-text::-webkit-input-placeholder',
							'property'					=> 'color'
						),
					),
					'input_typography'		=> array(
						'type'        	    		=> 'typography',
						'label'       	    		=> __( 'Typography', 'woopack' ),
						'responsive'  	    		=> true,
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .form-row .input-text, .woopack-my-account .woocommerce form .form-row .select2-container .select2-selection--single',
						),
					),
					'input_desc_typography'		=> array(
						'type'        	    		=> 'typography',
						'label'       	    		=> __( 'Description Typography', 'woopack' ),
						'responsive'  	    		=> true,
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .form-row span',
						),
					),
				),
			),
			'labels_style'        => array(
				'title'             => __( 'Labels', 'woopack' ),
				'collapsed'	=> true,
				'fields'            => array(
					'label_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Text Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .form-row label',
							'property'					=> 'color'
						),
					),
					'label_margin'    	=> array(
						'type' 							=> 'unit',
						'label' 						=> __( 'Margin Bottom', 'woopack' ),
						'default' 						=> '',
						'units'							=> array( 'px' ),
						'slider'						=> true,
						'responsive' 					=> true,
						'preview'           			=> array(
							'type'              			=> 'css',
							'selector'          			=> '.woopack-my-account .woocommerce .form-row label',
							'property'          			=> 'margin-bottom',
							'unit'							=> 'px'
						),
					),
					'label_typography'		=> array(
						'type'        	    		=> 'typography',
						'label'       	    		=> __( 'Typography', 'woopack' ),
						'responsive'  	    		=> true,
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .form-row label',
						),
					),
				),
			),
			'form_buttons'	=> array(
				'title'	=> __( 'Buttons', 'woopack' ),
				'description' => __( 'These options can be used to override styling set under the Buttons tab', 'woopack' ),
				'collapsed'	=> true,
				'fields' => array(
					'form_button_alignment'		=> array(
						'type'              	=> 'align',
						'label'             	=> __('Alignment', 'woopack'),
						'default'           	=> '',
						'preview'           	=> array(
							'type'              	=> 'css',
							'selector'          	=> '.woopack-my-account .woocommerce .woocommerce-form-register .form-row:last-child, .woopack-my-account .woocommerce .woocommerce-form-login .form-row:last-child, .woopack-my-account .woocommerce form .woopack-my-account-button',
							'property'          	=> 'text-align',
						),
					),
					'form_button_width'			=> array(
						'type'                  => 'select',
						'label'                 => __('Width', 'woopack'),
						'default'               => 'auto',
						'options'               => array(
							'auto'                  => __('Auto', 'woopack'),
							'full_width'            => __('Full Width', 'woopack'),
							'custom'                => __('Custom', 'woopack'),
						),
						'toggle'                => array(
							'custom'                => array(
								'fields'                => array('form_button_width_custom')
							),
						),
					),
					'form_button_width_custom'	=> array(
						'type'              => 'unit',
						'label'                 => __('Custom Width', 'woopack'),
						'units'					=> array('%'),
						'slider'				=> true,
						'responsive' 			=> true,
						'preview'           	=> array(
							'type'              	=> 'css',
							'selector'          	=> '.woopack-my-account .woocommerce form .form-row button, .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button',
							'property'          	=> 'width',
							'unit'					=> '%'
						),
					),
					'form_button_margin_top'		=> array(
						'type'                  => 'unit',
						'label'                 => __('Margin Top', 'woopack'),
						'default'				=> '',
						'units'					=> array('px'),
						'slider'				=> true,
						'responsive'        	=> true,
					),
					'form_button_margin_bottom'	=> array(
						'type'                  => 'unit',
						'label'                 => __('Margin Bottom', 'woopack'),
						'default'				=> '',
						'units'					=> array('px'),
						'slider'				=> true,
						'responsive'        	=> true,
					),
					'form_button_border_group'       => array(
						'type'                      => 'border',
						'label'                     => __('Border Style', 'woopack'),
						'responsive'                => true,
						'preview'                   => array(
							'type'                      => 'css',
							'selector' 		            => '.woopack-my-account .woocommerce form .form-row button, .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button',
						),
					),
					'form_button_border_color_hover'	=> array(
						'type' 						=> 'color',
						'label' 					=> __('Border Hover Color', 'woopack'),
						'show_reset' 				=> true,
					),
					'form_button_padding'    => array(
						'type'              	=> 'dimension',
						'label' 				=> __('Padding', 'woopack'),
						'units'					=> array('px'),
						'slider'				=> true,
						'preview' 				=> array(
							'type' 					=> 'css',
							'selector'				=> '.woopack-my-account .woocommerce form .form-row button, .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button',
							'property'				=> 'padding',
							'unit' 					=> 'px'
						),
						'responsive' 			=> true,
					),
					'form_button_bg_color'       => array(
						'type'              	=> 'color',
						'label'             	=> __('Background Color', 'woopack'),
						'show_reset'        	=> true,
						'show_alpha'        	=> true,
						'preview'           	=> array(
							'type' 					=> 'css',
							'selector'				=> '.woopack-my-account .woocommerce form .form-row button, .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button',
							'property'				=> 'background-color',
						),
					),
					'form_button_bg_color_hover'	=> array(
						'type'              	=> 'color',
						'label'             	=> __('Background Hover Color', 'woopack'),
						'show_reset'        	=> true,
						'show_alpha'        	=> true,
					),
					'form_button_color'          => array(
						'type'              	=> 'color',
						'label'             	=> __('Text Color', 'woopack'),
						'show_reset'        	=> true,
						'preview'           	=> array(
							'type'              	=> 'css',
							'selector'          	=> '.woopack-my-account .woocommerce form .form-row button, .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button',
							'property'          	=> 'color',
						),
					),
					'form_button_color_hover'    => array(
						'type'              	=> 'color',
						'label'             	=> __('Text Hover Color', 'woopack'),
						'show_reset'        	=> true,
					),
					'form_button_typography' => array(
						'type'        	    => 'typography',
						'label'       	    => __( 'Button Typography', 'woopack' ),
						'responsive'  	    => true,
						'preview'           => array(
							'type'              => 'css',
							'selector' 		    => '.woopack-my-account .woocommerce form .form-row button, .woopack-my-account .woocommerce .woocommerce-MyAccount-content form .button',
						),
					),
				)
			)
		),
	),
	'notices'        => array(
		'title'         => __( 'Notices', 'woopack' ),
		'description'	=> __( 'Controls styling for errors and general notices.', 'woopack' ),
		'sections'      => array(
			'errors_style'	=> array(
				'title'	=> __( 'Errors', 'woopack' ),
				'fields' => array(
					'error_message_bg_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Background Color', 'woopack' ),
						'show_reset'                    => true,
						'show_alpha'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-error',
							'property'					=> 'background-color',
						),
					),
					'error_message_text_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Text Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-error',
							'property'					=> 'color'
						),
					),
					'error_message_icon_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Icon Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-error:before',
							'property'					=> 'color'
						),
					),
					'error_message_border'	=> array(
						'type'					=> 'border',
						'label'					=> __( 'Border', 'woopack' ),
						'responsive'			=> true,
						'preview'				=> array(
							'type'					=> 'css',
							'selector'				=> '.woopack-my-account .woocommerce .woocommerce-error',
						),
					),
				),
			),
			'message_style'	=> array(
				'title'	=> __( 'General Notices', 'woopack' ),
				'collapsed'	=> true,
				'fields' => array(
					'message_info_bg_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Background Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-info',
							'property'					=> 'background-color'
						),
					),
					'message_info_text_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Text Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-info',
							'property'					=> 'color'
						),
					),
					'message_info_icon_color'              => array(
						'type'                          => 'color',
						'label'                         => __( 'Icon Color', 'woopack' ),
						'show_reset'                    => true,
						'connections'					=> array( 'color' ),
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-info:before',
							'property'					=> 'color'
						),
					),
					'message_info_border'	=> array(
						'type'					=> 'border',
						'label'					=> __( 'Border', 'woopack' ),
						'responsive'			=> true,
						'preview'				=> array(
							'type'					=> 'css',
							'selector'				=> '.woopack-my-account .woocommerce .woocommerce-info',
						),
					),
				),
			),
			'message_typography'	=> array(
				'title'	=> __( 'Typography', 'woopack' ),
				'collapsed'	=> true,
				'fields' => array(
					'notices_typography'		=> array(
						'type'        	    		=> 'typography',
						'label'       	    		=> __( 'Typography', 'woopack' ),
						'responsive'  	    		=> true,
						'preview'           		=> array(
							'type'              		=> 'css',
							'selector' 		    		=> '.woopack-my-account .woocommerce .woocommerce-error,
															.woopack-my-account .woocommerce .woocommerce-info,
															.woopack-my-account .woocommerce .woocommerce-message',
						),
					),
				),
			),
		),
	),
));
