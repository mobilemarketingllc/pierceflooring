<?php
// ******************* Padding *******************
// Box Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'box_padding',
	'selector' 		=> ".fl-node-$id .woopack-single-product",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'box_padding_top',
		'padding-right' 	=> 'box_padding_right',
		'padding-bottom' 	=> 'box_padding_bottom',
		'padding-left' 		=> 'box_padding_left',
	),
) );
// Content Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'content_padding',
	'selector' 		=> ".fl-node-$id .woopack-single-product .product-content",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'content_padding_top',
		'padding-right' 	=> 'content_padding_right',
		'padding-bottom' 	=> 'content_padding_bottom',
		'padding-left' 		=> 'content_padding_left',
	),
) );
// Product Meta Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'meta_padding',
	'selector' 		=> ".fl-node-$id .woopack-single-product .product_meta",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'meta_padding_top',
		'padding-right' 	=> 'meta_padding_right',
		'padding-bottom' 	=> 'meta_padding_bottom',
		'padding-left' 		=> 'meta_padding_left',
	),
) );
// Button Padding
FLBuilderCSS::dimension_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'button_padding',
	'selector' 		=> ".fl-node-$id .woopack-single-product .woocommerce-product-add-to-cart a:not(.reset_variations),
						.fl-node-$id .woopack-single-product .woocommerce-product-add-to-cart .button,
						.fl-node-$id .woopack-single-product .woocommerce-product-add-to-cart .button.disabled,
						.fl-node-$id .woopack-single-product .woocommerce-product-add-to-cart .button.alt.disabled,
						.fl-node-$id .woopack-single-product .woocommerce-product-add-to-cart .button.single_add_to_cart_button.alt",
	'unit'			=> 'px',
	'props'			=> array(
		'padding-top' 		=> 'button_padding_top',
		'padding-right' 	=> 'button_padding_right',
		'padding-bottom' 	=> 'button_padding_bottom',
		'padding-left' 		=> 'button_padding_left',
	),
) );
// ******************* Border *******************
// Box Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'box_border_group',
	'selector' 		=> ".fl-node-$id .woopack-single-product",
) );
// Sale Badge Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'sale_badge_border_group',
	'selector' 		=> ".fl-node-$id .woopack-single-product .onsale",
) );
// Button Border - Settings
FLBuilderCSS::border_field_rule( array(
	'settings' 		=> $settings,
	'setting_name' 	=> 'button_border_group',
	'selector' 		=> ".fl-node-$id .woopack-single-product .woocommerce-product-add-to-cart a:not(.reset_variations),
						.fl-node-$id .woopack-single-product .woocommerce-product-add-to-cart .button,
						.fl-node-$id .woopack-single-product .woocommerce-product-add-to-cart .button.disabled,
						.fl-node-$id .woopack-single-product .woocommerce-product-add-to-cart .button.alt.disabled,
						.fl-node-$id .woopack-single-product .woocommerce-product-add-to-cart .button.single_add_to_cart_button.alt",
) );
// ******************* Typography *******************
// Button Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'button_typography',
	'selector' 		=> ".fl-node-$id .woopack-single-product .woocommerce-product-add-to-cart .add_to_cart_inline a, 
						.fl-node-$id .woopack-single-product .woocommerce-product-add-to-cart a, 
						.fl-node-$id .woopack-single-product .woocommerce-product-add-to-cart .button",
) );
// Title Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'product_title_typography',
	'selector' 		=> ".fl-node-$id .woopack-single-product .woopack-product-title",
) );
// Regular Price Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'regular_price_typography',
	'selector' 		=> ".fl-node-$id .woopack-single-product .price .amount",
) );
// Short Description Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'short_description_typography',
	'selector' 		=> ".fl-node-$id .woopack-single-product .woocommerce-product-details__short-description,
						.fl-node-$id .woopack-single-product .woocommerce-product-details__short-description p,
						.fl-node-$id .woopack-single-product .woocommerce-product-add-to-cart .variations_form.cart,
						.fl-node-$id .woopack-single-product .woopack-single-product .woocommerce-product-add-to-cart label",
) );
// Sale Badge Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'sale_badge_typography',
	'selector' 		=> ".fl-node-$id .woopack-single-product .onsale",
) );
// Product Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'meta_typography',
	'selector' 		=> ".fl-node-$id .woopack-single-product .product_meta",
) );

?>

<?php $product_id = $settings->product_id; ?>
.fl-node-<?php echo $id; ?> .woopack-single-product {
	position: relative;
	<?php WooPack_Helper::print_css( 'background-color', $settings->content_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'text-align', $settings->content_alignment, '', 'default' != $settings->content_alignment ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->content_bg_color_hover ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product a {
	text-decoration: none;
	position: relative;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .product-img-wrapper {
	vertical-align: top;
}
<?php if ( 1 == $settings->product_layout || 2 == $settings->product_layout ) { ?>
	.fl-node-<?php echo $id; ?> .woopack-single-product .summary {
		display: flex;
		justify-content: space-evenly;
		flex-direction: row;
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .single-product-image {
		width: 30%
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .product-content {
		width: 70%;
	}
<?php } ?>

.fl-node-<?php echo $id; ?> .woopack-single-product .onsale {
	position: absolute;
	<?php if ( 'top-left' == $settings->badge_position ) { ?>
		top: 0 !important;
		left: 0 !important;
		bottom: auto !important;
		right: auto !important;
	<?php } elseif ( 'top-center' == $settings->badge_position ) { ?>
		top: 0 !important;
		left: 50% !important;
		bottom: auto !important;
		right: auto !important;
		transform: translateX(-50%);
	<?php } elseif ( 'top-right' == $settings->badge_position ) { ?>
		top: 0 !important;
		left: auto !important;
		bottom: auto !important;
		right: 0 !important;
	<?php } elseif ( 'bottom-left' == $settings->badge_position ) { ?>
		top: auto !important;
		left: 0 !important;
		bottom: 0 !important;
		right: auto !important;
	<?php } elseif ( 'bottom-center' == $settings->badge_position ) { ?>
		top: auto !important;
		left: 50% !important;
		bottom: 0 !important;
		right: auto !important;
		transform: translateX(-50%);
	<?php } elseif ( 'bottom-right' == $settings->badge_position ) { ?>
		top: auto !important;
		left: auto !important;
		bottom: 0 !important;
		right: 0 !important;
	<?php } ?>

	<?php WooPack_Helper::print_css( 'background-color', $settings->badge_bg_color ); ?>

	<?php WooPack_Helper::print_css( 'padding-top', $settings->badge_padding_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-bottom', $settings->badge_padding_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-left', $settings->badge_padding_left_right, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'padding-right', $settings->badge_padding_left_right, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'margin-top', $settings->badge_margin_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->badge_margin_top_bottom, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-left', $settings->badge_margin_left_right, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-right', $settings->badge_margin_left_right, 'px' ); ?>

	<?php WooPack_Helper::print_css( 'color', $settings->badge_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .product-content {
	<?php if ( 1 == $settings->product_layout ) { ?>
		float: left;
	<?php } elseif ( 2 == $settings->product_layout ) { ?>
		float: right;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woopack-product-title {
	<?php WooPack_Helper::print_css( 'color', $settings->product_title_color ); ?>
	margin-top: 0;
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_title_margin, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating {
	<?php WooPack_Helper::print_css( 'text-align', $settings->content_alignment, '', 'default' != $settings->content_alignment ); ?>
	<?php WooPack_Helper::print_css( 'font-size', $settings->product_rating_size, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_rating_margin, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating .star-rating {
	float: none;
	<?php if ( 'center' == $settings->content_alignment ) { ?>
	margin: 0 auto;
	<?php } ?>
	<?php if ( 'right' == $settings->content_alignment ) { ?>
		float: right;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating .star-rating::before {
	<?php WooPack_Helper::print_css( 'color', $settings->product_rating_default_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating .star-rating span::before {
	<?php WooPack_Helper::print_css( 'color', $settings->product_rating_color ); ?>
}

.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating .woocommerce-rating-count {
	<?php WooPack_Helper::print_css( 'color', $settings->product_rating_count_color ); ?>
}
<?php
// Product Rating Count Typography
FLBuilderCSS::typography_field_rule( array(
	'settings'		=> $settings,
	'setting_name' 	=> 'rating_count_taxonomy',
	'selector' 		=> ".fl-node-$id .woopack-single-product .woocommerce-product-rating .woocommerce-rating-count",
) );

?>
.fl-node-<?php echo $id; ?> .woopack-single-product .price {
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_price_margin, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .price .amount {
	<?php WooPack_Helper::print_css( 'color', $settings->regular_price_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .price ins {
	text-decoration: none;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .price ins .amount {
	text-decoration: none;
	<?php WooPack_Helper::print_css( 'font-size', $settings->sale_price_font_size_custom, 'px', 'custom' == $settings->sale_price_font_size ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->sale_price_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-details__short-description {
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_description_margin_bottom, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-details__short-description,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-details__short-description p {
	<?php WooPack_Helper::print_css( 'color', $settings->short_description_color ); ?>
}

/**************************
 * Variations
 **************************/
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations_form.cart,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart label {
	<?php WooPack_Helper::print_css( 'color', $settings->short_description_color ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations {
	margin-top: 30px;
	margin-bottom: 15px;
	<?php if ( 'center' == $settings->content_alignment || 'default' == $settings->content_alignment ) { ?>
		<?php if ( 1 != intval( $settings->product_layout ) && 2 != intval( $settings->product_layout ) ) { ?>
		margin-left: auto;
		margin-right: auto;
		<?php } ?>
	<?php } ?>
	<?php if ( 'right' == $settings->content_alignment ) { ?>
	margin-left: auto;
	<?php } ?>
	<?php if ( 'left' == $settings->content_alignment ) { ?>
	margin-right: auto;
	<?php } ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations td label {
	margin: 0;
	margin-right: 30px;
	vertical-align: middle;
	font-size: 15px;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations tr select {
	margin-right: 10px;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart form.cart,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart form.cart .single_variation_wrap .woocommerce-variation-add-to-cart {
	width: 100%;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations_form.cart {
	<?php WooPack_Helper::print_css( 'text-align', $settings->content_alignment, '', $settings->content_alignment != 'default' ); ?>
	<?php WooPack_Helper::print_css( 'text-align', '-moz-'.$settings->content_alignment, '', $settings->content_alignment != 'default' ); ?>
	display: block;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations_form.cart .single_variation_wrap {
	margin-top: 5px;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .quantity {
	vertical-align: top;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .variations_form.cart .variations_button {
	<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_medium, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_medium, 'px' ); ?>
}

<?php
// Product variations
include WOOPACK_DIR . 'includes/product-variations.css.php';
?>

/**************************
 * Button
 **************************/
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .woocommerce-variation {
	margin-bottom: 10px;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart {
	<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top, 'px' ); ?>
	<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom, 'px' ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart a:not(.reset_variations),
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .button,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .button.disabled,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .button.alt.disabled,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .button.single_add_to_cart_button.alt {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color ); ?>
	<?php WooPack_Helper::print_css( 'width', '100', '%', 'full_width' == $settings->button_width ); ?>
	<?php WooPack_Helper::print_css( 'width', $settings->button_width_custom, '%', 'custom' == $settings->button_width ); ?>

	transition: 0.2s ease-in-out;
}
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart a:not(.reset_variations):hover,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .button:hover,
.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart .button.single_add_to_cart_button.alt:hover {
	<?php WooPack_Helper::print_css( 'background-color', $settings->button_bg_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'color', $settings->button_color_hover ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->button_border_color_hover, '', '' != $settings->button_border_color_hover ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .product_meta {
	<?php WooPack_Helper::print_css( 'color', $settings->meta_text_color ); ?>
	<?php WooPack_Helper::print_css( 'border-color', $settings->meta_border_color, '', 'yes' == $settings->meta_border ); ?>
	<?php WooPack_Helper::print_css( 'border', '0', '', 'no' == $settings->meta_border ); ?>
}
.fl-node-<?php echo $id; ?> .woopack-single-product .product_meta .posted_in a {
	<?php WooPack_Helper::print_css( 'color', $settings->meta_link_color ); ?>
}


<?php
// *********************
// Media Query
// *********************
?>
@media only screen and (max-width: <?php echo $global_settings->medium_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woopack-single-product .onsale {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->badge_padding_top_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->badge_padding_top_bottom_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->badge_padding_left_right_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->badge_padding_left_right_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woopack-product-title {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_title_margin_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating {
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_rating_size_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_rating_margin_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .price {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_price_margin_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .price ins .amount {
		<?php WooPack_Helper::print_css( 'font-size', $settings->sale_price_font_size_custom_medium, 'px', 'custom' == $settings->sale_price_font_size ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-details__short-description {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_description_margin_bottom_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart {
		<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_medium, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_medium, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .product_meta {
		<?php WooPack_Helper::print_css( 'border', '0', '', 'no' == $settings->meta_border ); ?>
	}
}

@media only screen and (max-width: <?php echo $global_settings->responsive_breakpoint; ?>px) {
	.fl-node-<?php echo $id; ?> .woopack-single-product .single-product-image {
		float: none;
		display: block;
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .single-product-image img {
		width: 100%;
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .onsale {
		<?php WooPack_Helper::print_css( 'padding-top', $settings->badge_padding_top_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-bottom', $settings->badge_padding_top_bottom_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-left', $settings->badge_padding_left_right_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'padding-right', $settings->badge_padding_left_right_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .product-content {
		width: 100% !important;
		float: none;
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woopack-product-title {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_title_margin_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-rating {
		<?php WooPack_Helper::print_css( 'font-size', $settings->product_rating_size_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_rating_margin_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .price {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_price_margin_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .price ins .amount {
		<?php WooPack_Helper::print_css( 'font-size', $settings->sale_price_font_size_custom_responsive, 'px', 'custom' == $settings->sale_price_font_size ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-details__short-description {
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->product_description_margin_bottom_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .woocommerce-product-add-to-cart {
		<?php WooPack_Helper::print_css( 'margin-top', $settings->button_margin_top_responsive, 'px' ); ?>
		<?php WooPack_Helper::print_css( 'margin-bottom', $settings->button_margin_bottom_responsive, 'px' ); ?>
	}
	.fl-node-<?php echo $id; ?> .woopack-single-product .product_meta {
		<?php WooPack_Helper::print_css( 'border', '0', '', 'no' == $settings->meta_border ); ?>
	}
}
