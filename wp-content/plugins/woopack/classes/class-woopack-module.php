<?php
/**
 * Handles logic for modules.
 *
 * @package WooPack
 * @since 2.6.10
 */

/**
 * Exit if accessed directly.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @class WooPack_Module.
 */
final class WooPack_Module {
	static private $add_to_cart_text = '';
	static private $is_variation_enabled = false;

	/**
	 * @since 2.7.0
	 * @return void
	 */
	static public function init() {
		add_action( 'plugins_loaded', __CLASS__ . '::init_hooks', 99 );
	}

	static public function init_hooks() {
		if ( class_exists( 'FLThemeBuilderLoader' ) ) {
			add_action( 'wp_enqueue_scripts', 					__CLASS__ . '::enqueue_scripts' );
			add_action( 'after_setup_theme', 					__CLASS__ . '::after_setup_theme' );
			add_action( 'fl_page_data_add_properties', 			__CLASS__ . '::add_field_connection' );
			add_action( 'woopack_loop_before_product_button',	__CLASS__ . '::add_filter_add_to_cart_text', 10, 2 );
			add_action( 'woopack_loop_after_product_button',	__CLASS__ . '::remove_filter_add_to_cart_text', 10, 2 );
			// Filters.
			add_filter( 'fl_builder_register_settings_form',   	__CLASS__ . '::product_module_settings', 10, 2 );
			add_filter( 'fl_builder_render_css',               	__CLASS__ . '::product_module_css', 10, 2 );
			add_filter( 'woopack_products_grid_layout_path', 	__CLASS__ . '::product_module_layout_path', 10, 3 );
			add_filter( 'woopack_products_carousel_layout_path', 	__CLASS__ . '::product_module_layout_path', 10, 3 );
			add_filter( 'woopack_product_custom_layout_html', 	__CLASS__ . '::custom_html_parse_shortcodes', 1 );
			add_filter( 'fl_builder_register_module', __CLASS__ . '::enable_woo_themer_modules', 10, 2 );

			if ( class_exists( 'FLPageData' ) ) {
				FLPageData::add_group( 'woopack', array(
					'label' => __( 'WooPack', 'woopack' ),
				) );
			}
		}
	}

	static public function enqueue_scripts() {
		if ( FLBuilderModel::is_builder_active() ) {
			wp_enqueue_style( 'woopack-product-module-settings', WOOPACK_URL . 'assets/modules/product-module-settings.css', array(), WOOPACK_VER );
			wp_enqueue_script( 'woopack-product-module-settings', WOOPACK_URL . 'assets/modules/product-module-settings.js', array('jquery'), WOOPACK_VER, true );
		}
	}

	static public function after_setup_theme() {
		add_filter( 'fl_theme_builder_part_hooks', __CLASS__ . '::register_part_hooks' );
	}

	/**
	 * Adds the custom code settings for custom post
	 * module layouts.
	 *
	 * @since 1.0
	 * @param array $form	Module setting form fields array.
	 * @param string $slug	Module slug.
	 * @return array
	 */
	static public function product_module_settings( $form, $slug ) {
		if ( ! in_array( $slug, array( 'product-grid', 'product-carousel' ), true ) ) {
			return $form;
		}

		$layout_fields = array_merge(
			array(
				'layout'	=> array(
					'type'		=> 'select',
					'label'		=> __( 'Layout', 'woopack' ),
					'default'	=> 'pre-defined',
					'options'	=> array(
						'pre-defined'	=> __( 'Pre-defined', 'woopack' ),
						'custom'		=> __( 'Custom', 'woopack' ),
					),
					'toggle'	=> array(
						'pre-defined'	=> array(
							'fields'		=> array( 'product_layout' ),
						),
						'custom'	=> array(
							'fields'	=> array( 'custom_layout' ),
						),
					),
				),
				'custom_layout'	=> array(
					'type'          => 'form',
					'label'         => __( 'Custom Layout', 'woopack' ),
					'form'          => 'woopack_product_custom_layout',
					'preview_text'  => null,
					'multiple'		=> false,
				),
			),
			$form['general']['sections']['layout']['fields']
		);

		$form['general']['sections']['layout']['fields'] = $layout_fields;

		FLBuilder::register_settings_form( 'woopack_product_custom_layout', array(
			'title' => __( 'Custom Layout', 'woopack' ),
			'tabs'  => array(
				'html'          => array(
					'title'         => __( 'HTML', 'woopack' ),
					'sections'      => array(
						'html'          => array(
							'title'         => '',
							'fields'        => array(
								'html'          => array(
									'type'          => 'code',
									'editor'        => 'html',
									'label'         => '',
									'rows'          => '18',
									'default'       => self::get_preset_data( 'html' ),
									'preview'           => array(
										'type'              => 'none',
									),
									'connections'       => array( 'html', 'string', 'url' ),
								),
							),
						),
					),
				),
				'css'           => array(
					'title'         => __( 'CSS', 'woopack' ),
					'sections'      => array(
						'css'           => array(
							'title'         => '',
							'fields'        => array(
								'css'           => array(
									'type'          => 'code',
									'editor'        => 'css',
									'label'         => '',
									'rows'          => '18',
									'default'       => self::get_preset_data( 'css' ),
									'preview'           => array(
										'type'              => 'none',
									),
								),
							),
						),
					),
				),
			),
		));

		return $form;
	}

	/**
	 * Get content from Post module's HTML and CSS files.
	 *
	 * @since 1.0
	 * @param string $type	html or css.
	 * @return string
	 */
	static public function get_preset_data( $type ) {
		$content = '';

		if ( ! in_array( $type, array( 'html', 'css' ) ) ) {
			return $content;
		}

		$file = WOOPACK_DIR . 'includes/custom-product-layout-' . $type . '.php';

		if ( file_exists( $file ) ) {
			$content = file_get_contents( $file );
		}

		return $content;
	}

	/**
	 * Renders custom CSS for the post grid module.
	 *
	 * @since 1.0
	 * @param string $css
	 * @param array  $nodes
	 * @return string
	 */
	static public function product_module_css( $css, $nodes ) {
		if ( ! class_exists( 'lessc' ) ) {
			require_once FL_THEME_BUILDER_DIR . 'classes/class-lessc.php';
		}
		foreach ( $nodes['modules'] as $module ) {

			if ( ! is_object( $module ) ) {
				continue;
			}
			if ( ! in_array( $module->settings->type, array( 'product-grid', 'product-carousel' ) ) ) {
				continue;
			}
			if ( ! isset( $module->settings->layout ) || 'custom' != $module->settings->layout ) {
				continue;
			}

			try {
				$less    = new lessc;
				$custom  = '.fl-node-' . $module->node . ' .woocommerce { ';
				$custom .= $module->settings->custom_layout->css;
				$custom .= ' }';
				if ( method_exists( 'FLBuilder', 'maybe_do_shortcode' ) ) {
					$custom = FLBuilder::maybe_do_shortcode( $custom );
				}
				$css .= @$less->compile( $custom ); // @codingStandardsIgnoreLine
			} catch ( Exception $e ) {
				$css .= $module->settings->custom_layout->css;
			}
		}
		return $css;
	}

	/**
	 * Get product custom layout path.
	 *
	 * @param string $path
	 * @param object $settings
	 * @param object $product
	 * @return string
	 */
	static public function product_module_layout_path( $path, $settings, $product ) {
		if ( isset( $settings->layout ) && 'custom' == $settings->layout ) {
			return WOOPACK_DIR . 'includes/custom-product-layout.php';
		}

		return $path;
	}

	static public function custom_html_parse_shortcodes( $content ) {
		return FLThemeBuilderFieldConnections::parse_shortcodes(
			$content,
			array(
				'wpbb-acf-flex',
				'wpbb-acf-repeater',
			)
		);
	}

	static public function register_part_hooks( $hooks ) {
		$hooks[] = array(
			'label'	=> 'WooPack',
			'hooks'	=> array(
				'woopack_quick_view_template'	=> __( 'Quick View Template', 'woopack' ),
			),
		);
		
		return $hooks;
	}

	static public function enable_woo_themer_modules( $enabled, $module ) {
		if ( ! is_user_logged_in() || ! isset( $_GET['fl_builder'] ) ) {
			return $enabled;
		}

		if ( is_callable( 'FLThemeBuilderLayoutData::current_post_is' ) && ! $enabled ) {
			if ( ! FLThemeBuilderLayoutData::current_post_is( 'part' ) ) {
				return $enabled;
			}

			if ( 'woopack_quick_view_template' !== get_post_meta( get_the_ID(), '_fl_theme_layout_hook', true ) ) {
				return $enabled;
			}

			$enabled = isset( $module->dir ) && strpos( $module->dir, 'extensions/woocommerce/modules' ) !== false;
		}

		return $enabled;
	}

	static public function add_field_connection() {
		// Product Image with gallery slider.
		FLPageData::add_post_property( 'woopack_product_images', array(
			'label'	=> __( 'Product Image with Gallery Slider', 'woopack' ),
			'group'	=> 'woopack',
			'type'	=> 'html',
			'getter'	=> 'WooPack_Module::get_product_images',
		) );

		FLPageData::add_post_property_settings_fields( 'woopack_product_images', array(
			'image_size'	=> array(
				'type'          => 'photo-sizes',
				'label'         => __( 'Size', 'woopack' ),
				'default'       => 'medium',
			),
			'image_slider'	=> array(
				'type'              => 'select',
				'label'             => __( 'Enable Image Slider?', 'woopack' ),
				'help'				=> __( 'This will enable product images slider on hover. It only supports Product Grid module at the moment.', 'woopack' ),
				'default'           => 'no',
				'options'           => array(
					'yes'               => __( 'Yes', 'woopack' ),
					'no'                => __( 'No', 'woopack' ),
				),
				'preview'			=> array(
					'type'				=> 'none'
				),
			),
			'show_sale_badge'	=> array(
				'type'              => 'select',
				'label'             => __( 'Show Sale Badge?', 'woopack' ),
				'default'           => 'yes',
				'options'           => array(
					'yes'               => __( 'Yes', 'woopack' ),
					'no'                => __( 'No', 'woopack' ),
				),
			),
		) );

		// Rating and Reviews count.
		FLPageData::add_post_property( 'woopack_product_rating', array(
			'label'  => __( 'Rating and Reviews Count', 'woopack' ),
			'group'  => 'woopack',
			'type'   => 'html',
			'getter' => 'WooPack_Module::get_product_ratings',
		) );

		FLPageData::add_post_property_settings_fields( 'woopack_product_rating', array(
			'show_rating'	=> array(
				'type'			=> 'select',
				'label'			=> __( 'Show Rating', 'woopack' ),
				'default'		=> 'yes',
				'options'		=> array(
					'yes'			=> __( 'Yes', 'woopack' ),
					'no'			=> __( 'No', 'woopack' ),
				),
			),
			'show_count'	=> array(
				'type'			=> 'select',
				'label'			=> __( 'Show Reviews Count', 'woopack' ),
				'default'		=> 'yes',
				'options'		=> array(
					'yes'			=> __( 'Yes', 'woopack' ),
					'no'			=> __( 'No', 'woopack' ),
				),
				'toggle'		=> array(
					'yes'			=> array(
						'fields'		=> array(
							'text_singular',
							'text_plural',
						),
					),
				),
			),
			'text_singular'	=> array(
				'type'	=> 'text',
				'label'	=> __( 'Suffix Text - Singular', 'woopack' ),
				'default' => __( 'customer review', 'woopack' ),
			),
			'text_plural'	=> array(
				'type'	=> 'text',
				'label'	=> __( 'Suffix Text - Plural', 'woopack' ),
				'default' => __( 'customer reviews', 'woopack' ),
			),
		) );

		// Add to Cart.
		FLPageData::add_post_property( 'woopack_add_to_cart_button', array(
			'label'  => __( 'Add to Cart', 'woopack' ),
			'group'  => 'woopack',
			'type'   => 'html',
			'getter' => 'WooPack_Module::get_add_to_cart_button',
		) );

		FLPageData::add_post_property_settings_fields( 'woopack_add_to_cart_button', array(
			'text'	=> array(
				'type'		=> 'text',
				'label'		=> __( 'Button Text', 'woopack' ),
				'default'	=> __( 'Add to cart', 'woopack' ),
			),
			'qty_input'	=> array(
				'type'			=> 'select',
				'label'			=> __( 'Show Qantity Input', 'woopack' ),
				'default'		=> 'before_button',
				'options'		=> array(
					'before_button'		=> __( 'Before Button', 'woopack' ),
					'after_button'		=> __( 'After Button', 'woopack' ),
					'above_button'		=> __( 'Above Button', 'woopack' ),
					'no'				=> __( 'No', 'woopack' ),
				),
			),
			'qty_style'	=> array(
				'type'		=> 'select',
				'label'		=> __( 'Style', 'woopack' ),
				'default'	=> '',
				'options'	=> array(
					''			=> __( 'Default', 'woopack' ),
					'custom'	=> __( 'Custom', 'woopack' ),
				),
				'help'	=> __( 'If you don\'t see custom quantity input in builder, just refresh the page.', 'woopack' ),
			),
			'variation_fields'	=> array(
				'type'				=> 'select',
				'label'				=> __( 'Enable Variation Fields', 'woopack' ),
				'default'			=> 'no',
				'options'			=> array(
					'yes'				=> __( 'Yes', 'woopack' ),
					'no'				=> __( 'No', 'woopack' ),
				),
				'help'				=> __( 'This will display the product variation fields right in the product grid.', 'woopack' )
			),
		) );

		// Quick View
		FLPageData::add_post_property( 'woopack_quick_view_button', array(
			'label'  => __( 'Quick View', 'woopack' ),
			'group'  => 'woopack',
			'type'   => 'html',
			'getter' => 'WooPack_Module::get_quick_view_button',
		) );

		$templates = isset( $_GET['fl_builder'] ) ? WooPack_Helper::get_fl_theme_product_singular_layouts(1) : array();

		FLPageData::add_post_property_settings_fields( 'woopack_quick_view_button', array(
			'icon'	=> array(
				'type'			=> 'icon',
				'label'			=> __( 'Button Icon', 'woopack' ),
				'show_remove'	=> true,
			),
			'text'	=> array(
				'type'			=> 'text',
				'label'			=> __( 'Button Text', 'woopack' ),
				'default'		=> __( 'Quick View', 'woopack' ),
			),
			'template'	=> array(
				'type'		=> 'select',
				'label'		=> __( 'Quick View Template', 'woopack' ),
				'default'	=> '',
				'options'	=> $templates,
			),
		) );
	}

	static public function get_product_images( $settings ) {
		global $product;

		ob_start();
		?>
		<a href="<?php echo get_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
			<?php WooPack_Helper::get_template_file( 'loop-product-sale-badge', $settings, $product ); ?>
			<?php WooPack_Helper::render_product_images( $product, $settings ); ?>
			<?php WooPack_Helper::get_template_file( 'loop-product-quick-view', $settings, $product ); ?>
			<?php WooPack_Helper::get_template_file( 'loop-product-stock-status', $settings, $product ); ?>
		</a>
		<?php
		return ob_get_clean();
	}

	static public function get_product_ratings( $settings ) {
		global $product;

		if ( ! isset( $settings->show_rating ) ) {
			$settings->show_rating = 'yes';
		}
		if ( ! isset( $settings->show_count ) ) {
			$settings->show_count = 'yes';
		}
		if ( ! isset( $settings->text_singular ) ) {
			$settings->text_singular = '';
		}
		if ( ! isset( $settings->text_plural ) ) {
			$settings->text_plural = '';
		}
		
		$settings->product_rating = $settings->show_rating;
		$settings->product_rating_count = $settings->show_count;
		$settings->product_rating_text = $settings->text_singular;
		$settings->product_rating_text_plural = $settings->text_plural;
		
		ob_start();
		WooPack_Helper::get_template_file( 'loop-product-rating', $settings, $product );
		return ob_get_clean();
	}

	static public function get_add_to_cart_button( $settings ) {
		global $product;

		$settings->button_type = 'cart';

		ob_start();
		WooPack_Helper::get_template_file( 'loop-product-button', $settings, $product );
		return ob_get_clean();
	}

	static public function get_quick_view_button( $settings ) {
		global $product;

		if ( isset( $settings->template ) ) {
			$settings->quick_view_template = $settings->template;
		}
		$settings->show_quick_view = 'yes';
		$settings->quick_view_custom_icon = $settings->icon;
		$settings->quick_view_custom_text = $settings->text;

		ob_start();
		WooPack_Helper::get_template_file( 'loop-product-quick-view', $settings, $product );
		return ob_get_clean();
	}

	static public function add_filter_add_to_cart_text( $settings, $product ) {
		if ( isset( $settings->text ) && ! empty( $settings->text ) ) {
			self::$add_to_cart_text = $settings->text;
			self::$is_variation_enabled = 'yes' === $settings->variation_fields;
			add_filter( 'woocommerce_product_add_to_cart_text', __CLASS__ . '::filter_add_to_cart_text', 10, 2 );
			add_filter( 'woocommerce_product_single_add_to_cart_text', __CLASS__ . '::filter_add_to_cart_text', 10, 2 );
		}
	}

	static public function remove_filter_add_to_cart_text( $settings, $product ) {
		if ( isset( $settings->text ) && ! empty( $settings->text ) ) {
			self::$add_to_cart_text = '';
			self::$is_variation_enabled = false;
			remove_filter( 'woocommerce_product_add_to_cart_text', __CLASS__ . '::filter_add_to_cart_text', 10, 2 );
			remove_filter( 'woocommerce_product_single_add_to_cart_text', __CLASS__ . '::filter_add_to_cart_text', 10, 2 );
		}
	}

	static public function filter_add_to_cart_text( $text, $product ) {
		if ( 'simple' === $product->get_type() ) {
			return $product->is_purchasable() && $product->is_in_stock() ? self::$add_to_cart_text : $text;
		} elseif ( 'grouped' === $product->get_type() ) {
			return self::$is_variation_enabled ? self::$add_to_cart_text : $text;
		} else {
			return $product->is_purchasable() ? self::$add_to_cart_text : $text;
		}

		return $text;
	}
}

WooPack_Module::init();
