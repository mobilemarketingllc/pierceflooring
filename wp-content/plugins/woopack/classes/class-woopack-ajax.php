<?php

class WooPack_Ajax {

    /**
     * Initializes actions.
     *
     * @since 1.0.0
     * @return void
     */
    static public function init()
    {
		add_action( 'wp', 											__CLASS__ . '::get_ajax_posts' );
        add_action( 'wp_ajax_woopack_product_quick_view', 			__CLASS__ . '::get_product_quick_view' );
        add_action( 'wp_ajax_nopriv_woopack_product_quick_view', 	__CLASS__ . '::get_product_quick_view' );
        add_action( 'wp_ajax_woopack_add_to_cart', 					__CLASS__ . '::add_to_cart' );
        add_action( 'wp_ajax_nopriv_woopack_add_to_cart', 			__CLASS__ . '::add_to_cart' );
		add_action( 'woopack_before_quick_view_wrap', 				__CLASS__ . '::remove_woocommerce_data' );
		add_action( 'wp_ajax_woopack_get_fl_theme_layouts',			__CLASS__ . '::get_fl_theme_layouts' );
		add_action( 'wp_ajax_nopriv_woopack_get_fl_theme_layouts',	__CLASS__ . '::get_fl_theme_layouts' );

		add_filter( 'woocommerce_add_to_cart_fragments', 			__CLASS__ . '::update_cart_counter', 10, 1 );
	}
	
	static public function update_cart_counter( $fragements ) {
		$fragements['div.fl-module-content .cart-counter'] = '<span class="cart-counter">' . WC()->cart->get_cart_contents_count() . '</span>';
		return $fragements;
	}

    /**
     * Get Product Quick View.
     *
     * @since 1.0.0
     * @return void
     */
    static public function get_product_quick_view()
    {
        if ( ! isset( $_POST['product_id'] ) || empty( $_POST['product_id'] ) ) {
            return;
		}

		$response = array();
		$template = false;
		
		if ( isset( $_POST['template'] ) ) {
			$template = absint( $_POST['template'] );
		}

		$product_id = intval( $_POST['product_id'] );
		$node_id = $_POST['node_id'];

		wp( 'p=' . $product_id . '&post_type=product' );

		if ( have_posts() ) {
			ob_start();

			while( have_posts() ) {
				the_post();

				WooPack_Helper::get_template_file(
					'product-quick-view',
					new stdClass(),
					null,
					false,
					array(
						'node_id' => $node_id,
						'product_id' => $product_id,
						'template_id' => $template,
					)
				);

				if ( $template ) {
					woocommerce_photoswipe();
				}
			}
			$response['html'] = ob_get_clean();

			if ( $template ) {
				$templateCSS = isset( $_POST['templateCSS'] ) && $_POST['templateCSS'];
				$templateJS = isset( $_POST['templateJS'] ) && $_POST['templateJS'];

				if ( $templateCSS || $templateJS ) {
					WooPack_Helper::render_scripts_and_styles( $template );

					if ( $templateCSS ) {
						global $wp_styles;
						ob_start();
						if ( isset( $wp_styles ) ) {
							wp_print_styles( $wp_styles->queue );
						}
						
						$response['css'] = ob_get_clean();
					}

					if ( $templateJS ) {
						global $wp_scripts;
						ob_start();
						if ( isset( $wp_scripts ) ) {
							$wp_scripts->done[] = 'jquery';
							wp_print_scripts( $wp_scripts->queue );
						}

						$response['js'] = ob_get_clean();
					}
				}
			}
		}

        wp_send_json( $response );
    }

    static public function get_ajax_posts()
    {
		if ( ! isset( $_POST['woopack_action'] ) || empty( $_POST['woopack_action'] ) ) {
			return;
		}
		if ( 'woopack_grid_get_posts' != $_POST['woopack_action'] ) {
			return;
		}
		
		// Tell WordPress this is an AJAX request.
		if ( ! defined( 'DOING_AJAX' ) ) {
			define( 'DOING_AJAX', true );
		}

		$post_type = 'product';
        $settings = (object)$_POST['settings'];
        $response = array(
            'data'  => '',
            'pagination' => false,
		);

        global $wp_query;

        $args = array(
            'post_type'     => 'product',
            'post_status'   => 'publish',
            'ignore_sticky_posts'   => true,
            'woopack'       => true,
        );

		if ( 'main_query' !== $settings->data_source ) {
			// posts filter.
			if ( isset( $settings->posts_product ) ) {
				
				$ids = $settings->posts_product;
				$arg = 'post__in';

				if ( isset( $settings->posts_product_matching ) ) {
					if ( ! $settings->posts_product_matching ) {
						$arg = 'post__not_in';
					}
				}

				if ( ! empty( $ids ) ) {
					$args[ $arg ] = explode( ',', $ids );
				}
			}


			// author filter.
			if ( isset( $settings->users ) ) {
				
				$users = $settings->users;
				$arg = 'author__in';
				
				// Set to NOT IN if matching is present and set to 0.
				if ( isset( $settings->users_matching ) && ! $settings->users_matching ) {
					$arg = 'author__not_in';
				}

				if ( !empty( $users ) ) {
					if ( is_string( $users ) ) {
						$users = explode( ',', $users );
					}
					
					$args[$arg] = $users;
				}
			}

			if ( isset( $settings->posts_per_page ) ) {
				$args['posts_per_page'] = $settings->posts_per_page;
			}
		}

        if ( 'yes' === $settings->enable_filter && isset( $settings->filter_taxonomy ) && isset( $_POST['term'] ) ) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => $settings->filter_taxonomy,
                    'field'    => 'slug',
                    'terms'    => $_POST['term']
                )
            );
        } else if ( isset( $settings->offset ) ) {
            //$args['offset'] = $settings->offset;
		}
		
		if ( 'custom_query' === $settings->data_source ) {

			$taxonomies = FLBuilderLoop::taxonomies( $post_type );

			foreach ( $taxonomies as $tax_slug => $tax ) {

				$tax_value = '';
				$term_ids  = array();
				$operator  = 'IN';

				// Get the value of the suggest field.
				if ( isset( $settings->{'tax_' . $post_type . '_' . $tax_slug} ) ) {
					// New style slug.
					$tax_value = $settings->{'tax_' . $post_type . '_' . $tax_slug};
				} elseif ( isset( $settings->{'tax_' . $tax_slug} ) ) {
					// Old style slug for backwards compat.
					$tax_value = $settings->{'tax_' . $tax_slug};
				}

				// Get the term IDs array.
				if ( ! empty( $tax_value ) ) {
					$term_ids = explode( ',', $tax_value );
				}

				// Handle matching settings.
				if ( isset( $settings->{'tax_' . $post_type . '_' . $tax_slug . '_matching'} ) ) {

					$tax_matching = $settings->{'tax_' . $post_type . '_' . $tax_slug . '_matching'};

					if ( ! $tax_matching ) {
						// Do not match these terms.
						$operator = 'NOT IN';
					} elseif ( 'related' === $tax_matching ) {
						// Match posts by related terms from the global post.
						global $post;
						$terms 	 = wp_get_post_terms( $post->ID, $tax_slug );
						$related = array();

						foreach ( $terms as $term ) {
							if ( ! in_array( $term->term_id, $term_ids ) ) {
								$related[] = $term->term_id;
							}
						}

						if ( empty( $related ) ) {
							// If no related terms, match all except those in the suggest field.
							$operator = 'NOT IN';
						} else {

							// Don't include posts with terms selected in the suggest field.
							$args['tax_query'][] = array(
								'taxonomy'	=> $tax_slug,
								'field'		=> 'id',
								'terms'		=> $term_ids,
								'operator'  => 'NOT IN',
							);

							// Set the term IDs to the related terms.
							$term_ids = $related;
						}
					}
				}// End if().

				if ( ! empty( $term_ids ) ) {

					$args['tax_query'][] = array(
						'taxonomy'	=> $tax_slug,
						'field'		=> 'id',
						'terms'		=> $term_ids,
						'operator'  => $operator,
					);
				}
			}// End foreach().
		}

		$args['tax_query'][] = array(
			'taxonomy'	=> 'product_visibility',
			'field'	    => 'name',
			'terms'     => 'exclude-from-catalog',
			'operator'  => 'NOT IN',
		);

        if ( 'yes' == get_option( 'woocommerce_hide_out_of_stock_items' ) ) {
            $args['meta_query'][] = array(
                'key'       => '_stock_status',
                'value'     => 'instock',
                'compare'   => '='
            );
        }

		if ( 'main_query' !== $settings->data_source && 'acf_relationship' !== $settings->data_source ) {
			if ( isset( $settings->order ) ) {
				// Order by author.
				if ( 'author' === $settings->order_by ) {
					$args['orderby'] = array(
						'author' => $settings->order,
						'date' => $settings->order,
					);
				} else {
					$args['orderby'] = $settings->order_by;
					$args['order'] = $settings->order;
				}
			}
		}

        if ( isset( $settings->product_source ) ) {
            $args = self::get_conditional_args( $settings->product_source, $args );
        }

        if ( isset( $_POST['paged'] ) ) {
            $args['paged'] = absint( $_POST['paged'] );
        }

        if ( isset( $_POST['orderby'] ) ) {
            $orderby = esc_attr( $_POST['orderby'] );
            
            $args = self::get_conditional_args( $orderby, $args );
		}
		
		$args = apply_filters( 'woopack_grid_ajax_query_args', $args );

		do_action( 'woopack_grid_ajax_before_query', $settings );

		if ( isset( $args['settings'] ) ) {
			unset( $args['settings'] );
		}

		if ( 'main_query' !== $settings->data_source ) {
			$query = new WP_Query( $args );
		} else {
			$query = $wp_query;
			if ( method_exists( 'WC_Query', 'pre_get_posts' ) ) {
				WC()->query->pre_get_posts( $query );
			}

			$tax_query = $query->get( 'tax_query' );

			if ( ! is_array( $tax_query ) ) {
				$tax_query = array();
			}
			
			if ( isset( $args['tax_query'] ) ) {
				$query->set( 'tax_query', array_merge( $tax_query, $args['tax_query'] ) );
			}
	
			if ( isset( $_POST['paged'] ) ) {
				$query->set('paged', absint( wp_unslash( $_POST['paged'] ) ) );
			}

			if ( isset( $_POST['author_id'] ) && ! empty( $_POST['author_id'] ) ) {
				$query->set( 'author__in', array( absint( wp_unslash( $_POST['author_id'] ) ) ) );
			}

			$query = new WP_Query( $query->query_vars );
		}

		do_action( 'woopack_grid_ajax_after_query', $settings, $query );

        if ( $query->have_posts() ) :

            // create pagination.
            if ( $query->max_num_pages > 1 ) {
                ob_start();
               
                echo '<div class="fl-builder-pagination woopack-ajax-pagination">';
                WooPack_Helper::pagination( $query, $_POST['current_page'], $_POST['paged'] );
                echo '</div>';

                $response['pagination'] = ob_get_clean();
            }

            // posts query.
            ob_start();

            while( $query->have_posts() ) {

        		$query->the_post();

                $product = wc_get_product( get_the_ID() );

                if ( is_object( $product ) && $product->is_visible() ) {
					$product_data = $product->get_data();
					//include apply_filters( 'woopack_products_grid_layout_path', WOOPACK_DIR . 'templates/product-layout-' . $settings->product_layout . '.php' );
					include apply_filters( 'woopack_products_grid_layout_path', WooPack_Helper::get_template_file( 'product-layout-' . $settings->product_layout, $settings, $product, 1 ), $settings, $product );
                }
            }
            
            wp_reset_postdata();

            $response['data'] = do_shortcode( ob_get_clean() );

        else :
            $response['data'] = '<li>' . esc_html__('No posts found.', 'woopack') . '</li>';
        endif;

        wp_reset_query();

        wp_send_json( $response );
    }

    static public function get_conditional_args( $type, $args )
    {
        switch ( $type ) :
            case 'featured':
                $args['tax_query'][] = array(
                    'taxonomy'         => 'product_visibility',
                    'terms'            => 'featured',
                    'field'            => 'name',
                    'operator'         => 'IN',
                    'include_children' => false,
                );
                break;

            case 'popularity':
            case 'best_selling':
                $args = WooPack_Helper::best_selling_products( $args );
                break;

            case 'rating':
            case 'top_rated':
                $args = WooPack_Helper::top_rated_products( $args );
                break;

            case 'sale':
                $args = WooPack_Helper::sale_products( $args );
                break;

            case 'date':
                $args['orderby'] = 'date ID';
                $args['order'] = 'DESC';
                break;

            case 'price':
                $args['meta_key'] = '_price';
                $args['order'] = 'ASC';
                $args['orderby'] = 'meta_value_num';
                break;

            case 'price-desc':
                $args['meta_key'] = '_price';
                $args['order'] = 'DESC';
                $args['orderby'] = 'meta_value_num';
                break;

            default:
                break;

        endswitch;

        return $args;
	}
	
	/**
	 * AJAX add to cart.
	 * 
	 * @since 1.3.4.3
	 */
	static public function add_to_cart() {
		ob_start();

		// phpcs:disable WordPress.Security.NonceVerification.NoNonceVerification
		if ( ! isset( $_POST['product_id'] ) ) {
			return;
		}

		$product_id        = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['product_id'] ) );
		$product           = wc_get_product( $product_id );
		$quantity          = empty( $_POST['quantity'] ) ? 1 : wc_stock_amount( $_POST['quantity'] );
		$passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity );
		$product_status    = get_post_status( $product_id );
		$variation_id      = isset( $_POST['variation_id'] ) ? absint( $_POST['variation_id'] ) : 0;
		$variation         = array();

		// build product variation array.
		if ( isset( $_POST['variation'] ) && is_array( $_POST['variation'] ) ) {
			foreach ( $_POST['variation'] as $var ) {
				$var_array = explode( '|', $var );
				$variation[ $var_array[0] ] = $var_array[1];
			}
		}

		if ( $product && 'variation' === $product->get_type() ) {
			$variation_id = $product_id;
			$product_id   = $product->get_parent_id();
			$variation    = $product->get_variation_attributes();
		}

		if ( $passed_validation && false !== WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation ) && 'publish' === $product_status ) {

			do_action( 'woocommerce_ajax_added_to_cart', $product_id );

			if ( 'yes' === get_option( 'woocommerce_cart_redirect_after_add' ) ) {
				wc_add_to_cart_message( array( $product_id => $quantity ), true );
			}

			// Return fragments
			WC_Ajax::get_refreshed_fragments();

		} else {

			// If there was an error adding to the cart, redirect to the product page to show any errors
			$data = array(
				'error'       => true,
				'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id ),
			);

			wp_send_json( $data );
		}
		// phpcs:enable
	}

	static public function remove_woocommerce_data() {
		//  Removes Woocommerce notices.
		remove_action('woocommerce_before_single_product', 'wc_print_notices', 10);

		//  Removes Woocommerce Tabs.
		remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);

		// Remove up sells from after single product hook.
		remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);

		// Remove related products from after single product hook.
		remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
	}

	static public function get_fl_theme_layouts() {
		$content = '';

		if ( ! is_user_logged_in() ) {
			return;
		}

		$layouts = WooPack_Helper::get_fl_theme_product_singular_layouts(1);

		foreach ( $layouts as $id => $title ) {
			$content .= '<option value="' . $id . '">' . $title . '</option>';
		}

		wp_send_json_success( $content );
	}
}

WooPack_Ajax::init();
